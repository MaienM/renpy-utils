import pytest

import renpy_utils.runtime.sources.scan as testmodule
from renpy_utils.util import Bunch


@pytest.fixture
def scope():
	return {
		'foo': 1,
		'bar': 1,
		'foobar': 4,
		'Foo': 10,
		'foo2': {
			'foo': 10,
			'bar': 12,
			'foo3': Bunch(
				foo = 12,
				bar = 10,
			),
		},
	}


def scan_to_list(*args, **kwargs):
	""" Wrapper around scan, that produces results that are easier to test against. """
	results = testmodule.scan(*args, **kwargs)
	results = sorted(results, key = lambda m: m.path)
	return [(m.key, m.value, m.context) for m in results]


class TestScan(object):
	def test_key(self, scope):
		assert scan_to_list(lambda k, v: k[-1] == 'r', scope) == [
			('bar', 1, []),
			('foobar', 4, []),
		]

	def test_value(self, scope):
		assert scan_to_list(lambda k, v: v > 2, scope) == [
			('Foo', 10, []),
			('foobar', 4, []),
		]

	def test_both(self, scope):
		assert scan_to_list(lambda k, v: k[-1] == 'r' and v > 2, scope) == [
			('foobar', 4, []),
		]

	def test_list(self):
		assert scan_to_list(lambda k, v: v > 10, [12, 2, 4, 6, 20]) == [
			('0', 12, []),
			('4', 20, []),
		]

	def test_object(self):
		assert scan_to_list(lambda k, v: v > 5, Bunch(foo = 1, bar = 10)) == [
			('bar', 10, []),
		]

	def test_recurse_1(self, scope):
		assert scan_to_list(lambda k, v: v == 10, scope, 1) == [
			('Foo', 10, []),
			('foo', 10, ['foo2']),
		]

	def test_recurse_2(self, scope):
		assert scan_to_list(lambda k, v: v == 10, scope, 2) == [
			('Foo', 10, []),
			('foo', 10, ['foo2']),
			('bar', 10, ['foo2', 'foo3']),
		]

import pytest

import renpy_utils.runtime.sources.filter as testmodule


class TestBuildFilterRegex(object):
	def test_invalid_query(self):
		assert testmodule.build_filter_regex('foo') is None
		assert testmodule.build_filter_regex('/foo') is None
		assert testmodule.build_filter_regex('/foo/t') is None

	def test_foo_key(self):
		filter = testmodule.build_filter_regex('/foo/')
		assert filter is not None
		assert filter('foo')
		assert filter('foobar')
		assert not filter('FOO')
		assert not filter('bar')

	def test_foo_value(self):
		filter = testmodule.build_filter_regex('/foo/')
		assert filter is not None
		assert filter('foo')
		assert filter('foobar')
		assert not filter('FOO')
		assert not filter('bar')

	def test_foo_i_value_nonstring(self):
		class Foo:
			pass
		filter = testmodule.build_filter_regex('/foo/i')
		assert filter is not None
		assert filter(('foo',))
		assert filter(['bar', 'foobar'])
		assert filter(Foo())
		assert not filter(['bar'])

	def test_foo_endl(self):
		filter = testmodule.build_filter_regex('/foo$/')
		assert filter is not None
		assert filter('foo')
		assert not filter('foobar')

	def test_foo_i(self):
		filter = testmodule.build_filter_regex('/foo/i')
		assert filter is not None
		assert filter('foo')
		assert filter('FOO')
		assert not filter('bar')

	def test_num(self):
		filter = testmodule.build_filter_regex('/[0-9]+/')
		assert filter is not None
		assert filter('123')
		assert not filter('foo')

	def test_num_value_nonstring(self):
		filter = testmodule.build_filter_regex('/[0-9]+/')
		assert filter is not None
		assert filter(12)
		assert filter(12.34)
		assert filter(0xAB)
		assert not filter('hello')


class TestBuildFilterEval(object):
	def test(self):
		filter = testmodule.build_filter_eval('$ > 5')
		assert filter is not None
		assert filter(10)
		assert not filter(4)

	def test_invalid_query(self):
		assert testmodule.build_filter_eval('a') is None

	def test_syntax_error(self):
		with pytest.raises(SyntaxError):
			testmodule.build_filter_eval('$ foo bar')
		with pytest.raises(SyntaxError):
			testmodule.build_filter_eval('($ *** 3) > 2')


class TestBuildFilterContains(object):
	def test(self):
		filter = testmodule.build_filter_contains('foo')
		assert filter is not None
		assert filter('foobar')
		assert filter('FoObAr')
		assert not filter('bar')

	def test_value(self):
		filter = testmodule.build_filter_contains('foo')
		assert filter is not None
		assert filter('foobar')
		assert filter('FoObAr')
		assert not filter('bar')

	def test_value_nonstring(self):
		class Foo:
			pass
		filter = testmodule.build_filter_contains('foo')
		assert filter is not None
		assert filter(('Foo',))
		assert filter(['bar', 'foobar'])
		assert filter(Foo())
		assert not filter(['bar'])

	def test_value_number(self):
		filter = testmodule.build_filter_contains('10')
		assert filter is not None
		assert filter(10)
		assert filter(100)
		assert not filter(1)


class TestBuildFilterFuzzy(object):
	def test(self):
		assert testmodule.build_filter_fuzzy('/foo/').builder == testmodule.build_filter_regex
		assert testmodule.build_filter_fuzzy('/foo$/').builder == testmodule.build_filter_regex
		assert testmodule.build_filter_fuzzy('$ > 5').builder == testmodule.build_filter_eval
		assert testmodule.build_filter_fuzzy('/foo').builder == testmodule.build_filter_contains
		assert testmodule.build_filter_fuzzy('foo').builder == testmodule.build_filter_contains

	def test_invalid_query(self, monkeypatch):
		monkeypatch.setattr('renpy_utils.runtime.sources.filter.BUILD_FILTER_METHODS', [])
		with pytest.raises(ValueError):
			testmodule.build_filter_fuzzy('foo')

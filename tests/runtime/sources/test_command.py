import pytest
from unittest.mock import MagicMock

import renpy_utils.runtime.sources.command as testmodule
from renpy_utils.util import Bunch


class Lexer(object):
	"""
	A mock implementation of RenPy's Lexer.

	Examples
	--------
	>>> l = Lexer('foo bar')
	>>> l.rest()
	'foo bar'
	"""

	def __init__(self, text):
		self.text = text

	def rest(self):
		return self.text


@pytest.fixture(autouse = True)
def setup_commands_nocatch():
	for command in testmodule.console_commands.values():
		command.catch_exceptions = False


@pytest.fixture
def console_commands(monkeypatch):
	console_commands = {}
	monkeypatch.setattr('renpy_utils.runtime.sources.command.console_commands', console_commands)
	return console_commands


@pytest.fixture(autouse = True)
def console(monkeypatch, console_commands):
	console = Bunch(
		help = lambda l: None,
		config = Bunch(
			console_commands = console_commands,
		),
		store = Bunch(
			__builtins__ = 'lots of crud',
			foo = 'Lorem ipsum dolor sit amet',
			bar = list(range(10, 1000)),
			baz = ['hello world'],
			foobar = 1,
			barfoo = 5,
			foofoo = True,
			barbar = 'foo',
			bazbaz = Bunch(
				boo = 'hello',
				far = 'world',
				faz = 3,
			),
		),
		renpy = Bunch(
			quit = lambda: None,
		),
	)
	monkeypatch.setattr('renpy_utils.runtime.sources.command._console', console)
	return console


class TestRegisteredCommands(object):
	def test(self, monkeypatch):
		monkeypatch.undo()
		assert sorted(testmodule.console_commands.keys()) == sorted(['help', 'scan', 'forcequit', 'restart'])


class TestCommand(object):
	def test(self, monkeypatch, console_commands):
		@testmodule.command('foo <bar>: foos the bar')
		def test(l):
			return 'lorem' + l.rest()
		assert console_commands['foo'] == test
		assert test(Lexer('')) == 'lorem'
		assert test(Lexer(' ipsum')) == 'lorem ipsum'

	def test_error(self, monkeypatch):
		@testmodule.command('foo <bar>: foos the bar')
		def test(l):
			raise Exception('Something has gone wrong')
		output = test(Lexer(''))
		assert 'Exception' in output
		assert 'gone wrong' in output

	def test_error_nocatch(self, monkeypatch):
		@testmodule.command('foo <bar>: foos the bar', catch_exceptions = False)
		def test(l):
			raise ValueError()
		with pytest.raises(ValueError):
			test(Lexer(''))


class TestGetDocs(object):
	def test_doctring_no_sections(self):
		def test():
			""" Hello world. """
		assert testmodule.get_docs(test) == 'Hello world.'

	def test_doctring_no_usage_section(self):
		def test():
			"""
			Hello world.

			Examples
			--------
			>>> print(1)
			1
			"""
		assert testmodule.get_docs(test) == 'Hello world.'

	def test_doctring_usage_section(self):
		def test():
			"""
			Hello world.

			Usage
			-----
			Lorem ipsum.
			"""
		assert testmodule.get_docs(test) == 'Lorem ipsum.'

	def test_no_doctring(self):
		def test():
			pass
		test.help = 'test: hello world'
		assert testmodule.get_docs(test) == 'Hello world'

	def test_no_docstring_or_help(self):
		def test():
			pass
		assert testmodule.get_docs(test) == 'Unknown'


class TestCommandHelp(object):
	def test(self, console):
		console.help = lambda l: 'foo bar'
		assert testmodule.command_help(Lexer('')) == 'foo bar'
		assert testmodule.command_help(Lexer('help')) != 'foo bar'
		assert testmodule.command_help(Lexer('foo')) == 'Unknown command foo'

	def test_doc(self, monkeypatch):
		@testmodule.command('foo <bar>: foos the bar', catch_exceptions = False)
		def test(l):
			""" Doesn't actually foo any bars. """
			pass
		assert "Doesn't actually foo any bars." in testmodule.command_help(Lexer('foo'))

	def test_nodoc(self, monkeypatch):
		@testmodule.command('foo <bar>: foos the bar', catch_exceptions = False)
		def test(l):
			pass
		assert 'Foos the bar' in testmodule.command_help(Lexer('foo'))


class TestCommandScan(object):
	@staticmethod
	def get_keys(output):
		return [line.split(':', 1)[0] for line in output.split('\n') if ':' in line]

	def test_all(self):
		output = testmodule.command_scan(Lexer(''))
		assert self.get_keys(output) == sorted(['foo', 'bar', 'baz', 'foobar', 'barfoo', 'foofoo', 'barbar', 'bazbaz'])

	def test_foo(self):
		output = testmodule.command_scan(Lexer('foo'))
		assert self.get_keys(output) == sorted(['foo', 'foobar', 'barfoo', 'foofoo', 'barbar'])

	def test_digits(self):
		output = testmodule.command_scan(Lexer('/[0-9]/'))
		assert self.get_keys(output) == sorted(['bar', 'foobar', 'barfoo', 'bazbaz'])

	def test_more_than_1(self):
		output = testmodule.command_scan(Lexer('$ > 1'))
		assert self.get_keys(output) == sorted(['barfoo'])

	def test_no_hidden(self):
		output = testmodule.command_scan(Lexer('crud'))
		assert output == ''

	def test_max_lines(self, monkeypatch):
		monkeypatch.setattr('renpy_utils.runtime.sources.command.MAX_SCAN_LINES', 2)
		output = testmodule.command_scan(Lexer(''))
		assert self.get_keys(output) == ['bar']
		assert 'and 7 more' in output.split('\n')

	def test_max_lines_pagination(self, monkeypatch):
		monkeypatch.setattr('renpy_utils.runtime.sources.command.MAX_SCAN_LINES', 2)
		output = testmodule.command_scan(Lexer('-p 2'))
		assert self.get_keys(output) == ['barbar']
		assert 'and 7 more' in output.split('\n')

	def test_max_line_length(self, monkeypatch):
		monkeypatch.setattr('renpy_utils.runtime.sources.command.MAX_SCAN_LINE_LENGTH', 20)
		output = testmodule.command_scan(Lexer('/^bar$/'))
		assert self.get_keys(output) == ['bar']
		assert output.endswith('...')
		assert len(output) < 30

	def test_max_line_length_match_truncated(self, monkeypatch):
		monkeypatch.setattr('renpy_utils.runtime.sources.command.MAX_SCAN_LINE_LENGTH', 20)
		output = testmodule.command_scan(Lexer('881'))
		assert output != ''

	def test_filter_on_key_foo(self):
		output = testmodule.command_scan(Lexer('-f key foo'))
		assert self.get_keys(output) == sorted(['foo', 'foobar', 'barfoo', 'foofoo'])

	def test_filter_on_value_foo(self):
		output = testmodule.command_scan(Lexer('-f value foo'))
		assert self.get_keys(output) == ['barbar']

	def test_target_fa(self):
		output = testmodule.command_scan(Lexer('-t bazbaz fa'))
		assert self.get_keys(output) == sorted(['far', 'faz'])

	def test_recursive_list(self):
		output = testmodule.command_scan(Lexer('-r1 400'))
		assert self.get_keys(output) == sorted(['bar', 'bar.390', 'bar.400'])


class TestCommandForcequit(object):
	def test(self, console):
		mock = MagicMock()
		console.renpy.quit = mock
		testmodule.command_forcequit(Lexer(''))
		mock.assert_called_once_with()


class TestCommandRestart(object):
	def test(self, console):
		mock = MagicMock()
		console.renpy.quit = mock
		testmodule.command_restart(Lexer(''))
		mock.assert_called_once_with(relaunch = True)

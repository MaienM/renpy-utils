import importlib
import io
import random
import sys
import textwrap

import pytest

from renpy_utils.util import Bunch
from renpy_utils.parser import ModuleArgumentParser
import renpy_utils.runtime.build as testmodule


def load_script_as_module(tmpdir, script):
	""" Load a piece of python code as if it were imported as a module. """
	tmpdir.join('module_loaded_from_script.py').write(script)
	sys.path.append(tmpdir.strpath)
	importlib.invalidate_caches()
	try:
		module = importlib.import_module('module_loaded_from_script')
		importlib.reload(module)
		return module
	finally:
		sys.path.remove(tmpdir.strpath)


class TestParser(object):
	def test(self):
		ModuleArgumentParser(testmodule)


class TestMain(object):
	def test(self):
		with io.StringIO() as f:
			args = Bunch(out = f)
			testmodule.main(args)
			script = f.getvalue()
		assert script != ''
		assert 'init' in script
		assert 'command' in script
		assert 'scan' in script
		assert 'SaveReader' not in script
		assert 'Converter' not in script


class TestBuildPy(object):
	def test_inline_dependency(self, tmpdir):
		main_path = tmpdir.join('file.py')
		main_path.write(textwrap.dedent('''
			import otherfile
			import somedir.somefile
			from somedir.anotherfile import anotherfile_thing1

			mainfile_thing = 10
		'''))
		tmpdir.join('otherfile.py').write(textwrap.dedent('''
			otherfile_thing = 6
		'''))
		somedir = tmpdir.mkdir('somedir')
		somedir.join('__init__.py').write('')
		somedir.join('somefile.py').write(textwrap.dedent('''
			somefile_thing = 14
		'''))
		somedir.join('anotherfile.py').write(textwrap.dedent('''
			anotherfile_thing1 = 23
			anotherfile_thing2 = 32
		'''))

		script = testmodule.build_py(main_path.strpath)
		assert 'mainfile_thing' in script
		assert 'otherfile_thing' in script
		assert 'somefile_thing' in script
		assert 'anotherfile_thing1' in script
		assert 'anotherfile_thing2' in script

		module = load_script_as_module(tmpdir, script)
		assert module.mainfile_thing == 10
		assert module.otherfile.otherfile_thing == 6
		assert module.somedir.somefile.somefile_thing == 14
		assert module.anotherfile_thing1 == 23


class TestInlineRpy(object):
	def test_inline_py(self, tmpdir):
		main_path = tmpdir.join('file.rpy')
		main_path.write(textwrap.dedent('''
			{{{file.py}}}
		'''))
		tmpdir.join('file.py').write(textwrap.dedent('''
			import otherfile

			mainfile_thing = 10
		'''))
		tmpdir.join('otherfile.py').write(textwrap.dedent('''
			otherfile_thing = 6
		'''))

		script = testmodule.build_rpy(main_path.strpath)
		assert 'mainfile_thing' in script
		assert 'otherfile_thing' in script

		module = load_script_as_module(tmpdir, script)
		assert module.mainfile_thing == 10
		assert module.otherfile.otherfile_thing == 6

	def test_inline_rpy(self, tmpdir):
		main_path = tmpdir.join('file.rpy')
		main_path.write(textwrap.dedent('''
			{{{otherfile.rpy}}}
		'''))
		tmpdir.join('otherfile.rpy').write(textwrap.dedent('''
			otherfile_thing = 10
		'''))

		script = testmodule.build_rpy(main_path.strpath)
		assert 'otherfile_thing' in script

		module = load_script_as_module(tmpdir, script)
		assert module.otherfile_thing == 10

	def test_inline_other(self, tmpdir):
		data = bytes(random.getrandbits(8) for _ in range(2 ** 6))

		main_path = tmpdir.join('file.rpy')
		main_path.write(textwrap.dedent('''
			data = {{{file.dat}}}
		'''))
		tmpdir.join('file.dat').write(data, mode = 'wb')

		script = testmodule.build_rpy(main_path.strpath)
		module = load_script_as_module(tmpdir, script)
		assert module.data == data

	def test_inline_rpy_with_py(self, tmpdir):
		main_path = tmpdir.join('file.rpy')
		main_path.write(textwrap.dedent('''
			{{{otherfile.rpy}}}
		'''))
		tmpdir.join('otherfile.rpy').write(textwrap.dedent('''
			{{{file.py}}}
		'''))
		tmpdir.join('file.py').write(textwrap.dedent('''
			mainfile_thing = 10
		'''))

		script = testmodule.build_rpy(main_path.strpath)
		assert 'mainfile_thing' in script

		module = load_script_as_module(tmpdir, script)
		assert module.mainfile_thing == 10

	def test_circular_inline(self, tmpdir):
		main_path = tmpdir.join('file.rpy')
		main_path.write(textwrap.dedent('''
			{{{otherfile.rpy}}}
		'''))
		tmpdir.join('otherfile.rpy').write(textwrap.dedent('''
			{{{file.rpy}}}
		'''))

		script = testmodule.build_rpy(main_path.strpath)
		assert 'already inlined' in script

	def test_self_inline(self, tmpdir):
		main_path = tmpdir.join('file.rpy')
		main_path.write(textwrap.dedent('''
			{{{file.rpy}}}
		'''))

		script = testmodule.build_rpy(main_path.strpath)
		assert 'already inlined' in script

	def test_inline_does_not_exist(self, tmpdir):
		main_path = tmpdir.join('file.rpy')
		main_path.write(textwrap.dedent('''
			{{{otherfile.rpy}}}
		'''))

		with pytest.raises(testmodule.InliningError) as e:
			testmodule.build_rpy(main_path.strpath)
		assert 'otherfile.rpy' in str(e.value)

	def test_inline_rpy_newlines(self, tmpdir):
		main_path = tmpdir.join('file.rpy')
		main_path.write(textwrap.dedent('''
			{{{otherfile.rpy}}}
		'''))
		tmpdir.join('otherfile.rpy').write(textwrap.dedent('''
			otherfile_thing = 10
			otherfile_thing2 = 20
		'''))

		script = testmodule.build_rpy(main_path.strpath)
		assert '\n\n' not in script.strip()

		module = load_script_as_module(tmpdir, script)
		assert module.otherfile_thing == 10
		assert module.otherfile_thing2 == 20

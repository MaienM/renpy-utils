# -*- coding: future_fstrings -*-

import argparse
import os
import os.path
import sys

import pytest

from renpy_utils.parser import ModuleArgumentParser
import renpy_utils.runtime.install as testmodule
from renpy_utils.util import Bunch


@pytest.fixture
def folder(tmpdir):
	game1 = tmpdir.mkdir('Game 1')
	game1.mkdir('game')
	game1.mkdir('stuff').mkdir('game')
	game2 = tmpdir.mkdir('Game 2')
	game2.mkdir('v0.1').mkdir('game')
	game2.mkdir('v0.2').mkdir('game')
	return tmpdir


@pytest.fixture
def args(folder):
	return Bunch(
		max_depth = 1,
		print_paths = False,
		dry_run = False,
		force_write = False,
		paths = [folder.strpath],
	)


class TestParser(object):
	def test(self):
		ModuleArgumentParser(testmodule)


class TestReadableDir(object):
	def test_readable(self, tmpdir):
		assert testmodule.readable_dir(tmpdir.strpath) == tmpdir.strpath

	def test_notadir(self):
		with pytest.raises(argparse.ArgumentTypeError):
			testmodule.readable_dir(__file__)

	def test_nonexistant(self, tmpdir):
		fakedir = tmpdir.join('does', 'not', 'exist')
		with pytest.raises(argparse.ArgumentTypeError):
			testmodule.readable_dir(fakedir.strpath)

	@pytest.mark.skip(reason = 'need a folder that will work for this reliably')
	@pytest.mark.skipif(sys.platform == 'win32', reason = 'does not run on windows')
	def test_nopermission(self):
		with pytest.raises(argparse.ArgumentTypeError):
			testmodule.readable_dir('/root')


class TestMain(object):
	def test_d1(self, capsys, args, folder):
		testmodule.main(args)
		output = capsys.readouterr().out
		assert output == ''
		assert folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 1', 'stuff', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 2', 'v0.1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 2', 'v0.2', 'game', 'renpy-utils.rpy').exists()

	def test_d2(self, capsys, args, folder):
		args.max_depth = 2
		testmodule.main(args)
		output = capsys.readouterr().out
		assert output == ''
		assert folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 1', 'stuff', 'game', 'renpy-utils.rpy').exists()
		assert folder.join('Game 2', 'v0.1', 'game', 'renpy-utils.rpy').exists()
		assert folder.join('Game 2', 'v0.2', 'game', 'renpy-utils.rpy').exists()

	def test_path_relative(self, capsys, args, folder):
		args.paths = [os.path.relpath(folder.join('Game 2').strpath)]
		testmodule.main(args)
		output = capsys.readouterr().out
		assert output == ''
		assert not folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 1', 'stuff', 'game', 'renpy-utils.rpy').exists()
		assert folder.join('Game 2', 'v0.1', 'game', 'renpy-utils.rpy').exists()
		assert folder.join('Game 2', 'v0.2', 'game', 'renpy-utils.rpy').exists()

	def test_d1_dry(self, capsys, args, folder):
		args.dry_run = True
		testmodule.main(args)
		output = capsys.readouterr().out
		assert 'Game 1' in output
		assert 'Game 2' not in output
		assert 'stuff' not in output
		assert not folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 2', 'v0.1', 'game', 'renpy-utils.rpy').exists()

	def test_d2_dry(self, capsys, args, folder):
		args.max_depth = 2
		args.dry_run = True
		testmodule.main(args)
		output = capsys.readouterr().out
		assert 'Game 1' in output
		assert 'Game 2' in output
		assert 'v0.1' in output
		assert 'stuff' not in output
		assert not folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 1', 'stuff', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 2', 'v0.1', 'game', 'renpy-utils.rpy').exists()

	def test_d1_print(self, capsys, args, folder):
		args.print_paths = True
		testmodule.main(args)
		output = capsys.readouterr().out
		assert 'Game 1' in output
		assert 'Game 2' not in output
		assert 'stuff' not in output
		assert folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 2', 'v0.1', 'game', 'renpy-utils.rpy').exists()

	def test_update_skip(self, capsys, args, folder):
		testmodule.main(args)
		assert folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 2', 'v0.1', 'game', 'renpy-utils.rpy').exists()
		assert not folder.join('Game 2', 'v0.2', 'game', 'renpy-utils.rpy').exists()
		args.max_depth = 2
		args.print_paths = True
		testmodule.main(args)
		output = capsys.readouterr().out
		assert 'Game 1' not in output
		assert 'Game 2' in output
		assert folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		assert folder.join('Game 2', 'v0.1', 'game', 'renpy-utils.rpy').exists()
		assert folder.join('Game 2', 'v0.2', 'game', 'renpy-utils.rpy').exists()

	def test_update_compare_hash(self, capsys, args, folder):
		path = folder.join('Game 1', 'game', 'renpy-utils.rpy').strpath
		with open(path, 'w') as f:
			f.write('Foo')
		args.print_paths = True
		testmodule.main(args)
		output = capsys.readouterr().out
		assert 'Game 1' in output
		with open(path, 'r') as f:
			assert f.read() != 'Foo'

	def test_update_force(self, capsys, args, folder):
		testmodule.main(args)
		assert folder.join('Game 1', 'game', 'renpy-utils.rpy').exists()
		args.print_paths = True
		args.force_write = True
		testmodule.main(args)
		output = capsys.readouterr().out
		assert 'Game 1' in output

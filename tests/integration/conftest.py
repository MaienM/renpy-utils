# -*- coding: future_fstrings -*-

import plistlib
import re

from fixtures.fixture_capture import clear_recordings
from fixtures.fixture_capture import record_x11
from fixtures.fixture_game import console
from fixtures.fixture_game import console_run
from fixtures.fixture_game import game
from fixtures.fixture_game import game_install
from fixtures.fixture_pexpect import pexpect
from fixtures.fixture_x11 import display
from fixtures.fixture_x11 import x11


REGEX_VERSION = re.compile('[0-9.-]+')


def pytest_report_header(config):
	""" Add the RenPy version to the report header. """
	# Read the info from the osx plist file
	with open('/opt/renpy/renpy.app/Contents/Info.plist', 'rb') as f:
		renpy_info = plistlib.load(f)
	# Get the version name
	version_raw = renpy_info['CFBundleShortVersionString']
	match = REGEX_VERSION.search(version_raw)
	version = match and match.group(0) or version_raw

	return f"RenPy {version}"


__all__ = (
	clear_recordings,
	console,
	console_run,
	display,
	game,
	game_install,
	pexpect,
	pytest_report_header,
	record_x11,
	x11,
)

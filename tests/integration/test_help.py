import pytest


pytestmark = [pytest.mark.depends(on = 'test_build.py')]


class TestHelp(object):
	def test_help(self, record_x11, console_run):
		output = console_run('help')
		assert 'commands:' in output
		assert 'help' in output

	def test_help_custom_commands(self, record_x11, console_run):
		output = console_run('help')
		assert 'scan' in output
		assert 'forcequit' in output
		assert 'restart' in output

	def test_help_help(self, record_x11, console_run):
		output = console_run('help help')
		assert 'Usage: help' in output

	def test_help_scan(self, record_x11, console_run):
		output = console_run('help scan')
		assert 'Usage: scan' in output

	def test_help_forcequit(self, record_x11, console_run):
		output = console_run('help forcequit')
		assert 'Usage: forcequit' in output

	def test_help_restart(self, record_x11, console_run):
		output = console_run('help restart')
		assert 'Usage: restart' in output

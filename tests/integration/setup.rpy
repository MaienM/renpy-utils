init 2001 python:
	# Store the output of the last command in a file, so it can be used by the tests
	def run(self, *args, **kwargs):
		try:
			return self.oldrun(*args, **kwargs)
		finally:
			with open('/tmp/renpy-lastoutput', 'w') as f:
				f.write(self.history[-1].result or '')
	_console.DebugConsole.oldrun = _console.DebugConsole.run
	_console.DebugConsole.run = run

	# Disable the performance tests
	config.performance_test = False

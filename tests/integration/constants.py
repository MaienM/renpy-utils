import os


# The default sleep time for when waiting for something
# Needs to be higher on slower systems
DEFAULT_SLEEP = float(os.environ.get('RENPY_UTILS_INTEGRATION_SLEEP', 0.5))

# The location of the game to run the tests in
GAME_PATH = '/opt/renpy/the_question'

# Environment variables that the renpy commands use
GAME_ENV = {
	# Disable unnecessary features
	'RENPY_DISABLE_BACKUPS': 'I take responsibility for this.',
	'RENPY_SKIP_SPLASHSCREEN': '1',
	# Disable the sound through SDL, for 7.x compatibility
	'RENPY_DISABLE_SOUND': '',
	'SDL_AUDIODRIVER': 'dummy',
}

# The build to test
RPY_BUILD_PATH = 'renpy-utils.rpy'
# Util to capture console output for the tests
RPY_TEST_SETUP_PATH = 'tests/integration/setup.rpy'
# The files that should be installed into the game
INSTALL_FILES = [RPY_BUILD_PATH, RPY_TEST_SETUP_PATH]

# The location where the output of the last command will be written to
# Must match the path used in capture.rpy
OUTPUT_CAPTURE_PATH = '/tmp/renpy-lastoutput'

import os
import os.path
import subprocess

import pytest

from constants import GAME_ENV
from constants import GAME_PATH
from constants import RPY_BUILD_PATH
from constants import RPY_TEST_SETUP_PATH


class TestBuild(object):
	@pytest.mark.parametrize('game_install', [
		pytest.mark.depends(name = 'lint:build')([RPY_BUILD_PATH]),
		pytest.mark.depends(name = 'lint:setup')([RPY_TEST_SETUP_PATH]),
	], indirect = True)
	def test_lint(self, x11, game_install, request):
		""" Check whether all of the files we're going to use for the tests are linted successfully. """
		env = {}
		env.update(os.environ)
		env.update(GAME_ENV)
		output = subprocess.check_output(
			['renpy.sh', GAME_PATH, 'lint'],
			env = env,
			stderr = subprocess.STDOUT,
		)
		for path in game_install:
			name = os.path.basename(path)
			assert name not in str(output)

	@pytest.mark.parametrize('game_install', [
		pytest.mark.depends(name = 'run:build', on = ['lint:build'])([RPY_BUILD_PATH]),
		pytest.mark.depends(name = 'run:setup', on = ['lint:setup'])([RPY_TEST_SETUP_PATH]),
	], indirect = True)
	def test_run(self, game):
		""" Check whether the game can start successfully with each of the files we're going to use for the tests. """
		pass

	@pytest.mark.parametrize('game_install', [
		pytest.mark.depends(name = 'console:setup', on = ['lint:setup', 'run:setup'])([RPY_TEST_SETUP_PATH]),
		pytest.mark.depends(depends = ['test_lint', 'test_run', 'console:setup'])([RPY_BUILD_PATH, RPY_TEST_SETUP_PATH]),
	], indirect = True)
	def test_console_output(self, record_x11, console_run):
		""" Check whether the console output is captured successfully. """
		output = console_run('1 + 2')
		assert output == '3'

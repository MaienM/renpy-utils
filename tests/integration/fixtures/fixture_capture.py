# -*- coding: future_fstrings -*-

import os
import os.path

import pexpect
import pytest

from fixtures.util import attempt_n_times


RECORDING_PATH = os.path.realpath('recordings')


@pytest.fixture(scope = 'session', autouse = True)
def clear_recordings():
	""" Clear the old recordings, if any, and make sure the directory for recordings exists. """
	print('Clearing old recordings')
	os.makedirs(RECORDING_PATH, exist_ok = True)
	for file in os.listdir(RECORDING_PATH):
		os.remove(os.path.join(RECORDING_PATH, file))


@pytest.fixture
def record_x11(request, x11):
	display = x11['display']
	resolution = x11['resolution']

	# Start a recording with ffmpeg
	target = os.path.join(RECORDING_PATH, f'{request.node.nodeid.replace("::()", "")}.mp4')
	print('===== Starting FFmpeg to record the Xvfb display =====')
	print(f'Recording to {target}')
	child = pexpect.spawn(
		'ffmpeg',
		[
			# Grab display
			'-f', 'x11grab',
			'-video_size', f'{resolution[0]}x{resolution[1]}',
			'-i', f':{display}+0,0',
			# Recording options
			'-r', '12',  # 12 fps
			'-an',  # no audio
			'-pix_fmt', 'yuv420p',
			# Be less verbose
			'-hide_banner',
			'-nostats',
			# Output file per test
			'-y',  # overwrite if the file already exists
			target,
		],
	)
	try:
		# Wait for the recording to have been started
		needle = 'Press [q] to stop'
		try:
			child.expect_exact(needle, timeout = 5)
		except pexpect.exceptions.TIMEOUT:
			pytest.fail(f'Cannot find "{needle}" in the output of ffmpeg, it may not have started successfully', False)
		print('FFmpeg is now recording')

		# Wait for the test to run
		yield child
	finally:
		# Stop recording
		print('Stopping FFmpeg recording')
		try:
			# Stdout may be closed at this point, causing errors when trying to log
			# Disable the logfile to prevent this
			child.logfile = None

			if child.isalive():
				child.sendintr()
			for attempt in attempt_n_times():
				if not child.isalive():
					break
			else:
				pytest.fail(
					'FFmpeg refused to exit cleanly, resorting to killing it forcefully, recording may be lost',
					False
				)
		finally:
			child.terminate_or_raise()

# -*- coding: future_fstrings -*-

import os
import subprocess

import pytest
import Xlib.display

from fixtures.util import attempt_n_times
from fixtures.util import popen_terminate_with_signals
from fixtures.x11_util import Display


@pytest.fixture(scope = 'session')
def x11():
	"""
	A fixture that provides an X11 server.

	Yields
	------
	dict
		Information about the provided X11 server.

		display : str
			The display name of the X11 server.
		resolution : tuple
			The resolution of the X11 server (width, height).
	"""
	# Display settings
	display = '10'
	resolution = (1280, 1024)

	# Run Xvfb to serve as a virtual X11 display
	print('===== Starting Xvfb (virtual X11 display) =====')
	print(f'Display: {display}')
	print(f'Resolution: {resolution[0]}x{resolution[1]}')
	child = subprocess.Popen([
		'Xvfb',
		f':{display}',
		'-listen', 'tcp',
		'-auth', '/tmp/xvfb.auth',
		'-screen', '0', f'{resolution[0]}x{resolution[1]}x24',
		'-ac'
	])

	try:
		# Wait for the display socket to exist
		for attempt in attempt_n_times():
			# Check whether the display socket exists
			result = subprocess.run(['xwininfo', '-display', f':{display}', '-root', '-size'])

			# Make sure the process is still running
			if child.poll() is not None:
				pytest.fail(f'Xvfb failed to start (code {child.returncode})', False)

			# Display is running and socket has been found, so we can continue
			if result.returncode == 0:
				break
		else:
			pytest.fail('Xvfb display took too long to become available', False)
		print(f'Xvfb display is now available')

		# Set the environment variable
		os.environ['DISPLAY'] = f':{display}'

		# Yield info about the started display for use in the tests
		yield dict(display = display, resolution = resolution)

		# Clear the environment variable
		del os.environ['DISPLAY']
	finally:
		# Close Xvfb
		print('Stopping Xvfb display')
		popen_terminate_with_signals(child)


@pytest.fixture
def display(x11):
	"""
	A fixture that provides a Display instance for an X11 server.

	Returns
	-------
	fixture_x11.Display
		A wrapper around the X11 display handle.
	"""
	handle = Xlib.display.Display(f':{x11["display"]}')
	return Display(handle)

# -*- coding: future_fstrings -*-

import os
import os.path
import signal
import subprocess
import time

import pytest
import Xlib.X

from fixtures.util import attempt_n_times
from fixtures.util import popen_terminate_with_signals
from fixtures.x11_util import Window
from constants import DEFAULT_SLEEP
from constants import GAME_ENV
from constants import GAME_PATH
from constants import INSTALL_FILES
from constants import OUTPUT_CAPTURE_PATH


@pytest.fixture(scope = 'session')
def game_install(request):
	"""
	A fixture that installs the required files into the game, and cleans them up afterwards.

	Yields
	------
	list
		The installed files
	"""
	# Copy over files, replacing tabs with spaces
	files = getattr(request, 'param', INSTALL_FILES)
	installed = []
	for source in files:
		target = os.path.join(GAME_PATH, 'game', os.path.basename(source))
		with open(source, 'r') as sf, open(target, 'w') as tf:
			tf.write(sf.read().replace('\t', '  '))
		installed.append(target)

	# Yield the installed paths
	yield installed[:]

	# Clean up, including compiled versions of the files
	for path in installed:
		for suffix in ['', 'c']:
			try:
				os.remove(path + suffix)
			except FileNotFoundError:
				pass


@pytest.fixture
def game(display, game_install):
	"""
	A fixture that runs the game with the required files installed, and cleans it all up afterwards.

	Yields
	------
	dict
		Information about the running game.

		child : subprocess.Popen
			The Popen handle for the game process.
		window : fixture_x11.Window
			A wrapper around the X11 window handle.
	"""
	# Get the already existing windows
	existing_window_ids = [w.id for w in display.get_windows()]

	# Start the game
	env = {}
	env.update(os.environ)
	env.update(GAME_ENV)
	print("===== Starting RenPy =====")
	child = subprocess.Popen(['renpy.sh', GAME_PATH, 'run'], env = env)

	try:
		# Find the x11 window by looking for a new, visible window with the appropriate class
		renpy_window = None
		for attempt in attempt_n_times():
			if child.poll() is not None:
				pytest.fail(f"RenPy failed to start (code {child.returncode})", False)

			for window in display.get_windows():
				if window.id not in existing_window_ids \
						and window.get_attributes().map_state == Xlib.X.IsViewable \
						and window.get_wm_class()[0] == 'renpy':
					renpy_window = window
					break
			if renpy_window is not None:
				break
		else:
			pytest.fail("RenPy window took too long to appear", False)
		time.sleep(DEFAULT_SLEEP)

		yield dict(
			child = child,
			window = Window(display, renpy_window),
		)
	finally:
		# Kill the running process
		print("Stopping RenPy")
		popen_terminate_with_signals(child, signals = [signal.SIGTERM, signal.SIGKILL])


@pytest.fixture
def console(game):
	"""
	A fixture that provides access to the console of the game.

	This will start by opening the console in the game, so inputs can be sent to the console.
	"""
	print("Opening RenPy console")
	window = game['window']
	window.send_keypress(Xlib.XK.XK_Shift_L)
	window.send_key(Xlib.XK.XK_O)
	window.send_keyrelease(Xlib.XK.XK_Shift_L)
	time.sleep(DEFAULT_SLEEP)


@pytest.fixture
def console_run(game, console):
	"""
	A fixture that provides a function to interact with the console.

	Returns
	-------
	function
		A function to run a command on the console.
	"""
	window = game['window']

	def run(command, timeout = 5):
		"""
		Run a command on the console.

		Parameters
		----------
		command : str
			The command to run

		Returns
		-------
		str
			The output of the command.

			Contains both stdout and stderr.
		"""
		# Remove the results of the previous command
		try:
			os.remove(OUTPUT_CAPTURE_PATH)
		except FileNotFoundError:
			pass

		# Send the command
		print(f"Sending command to RenPy console: {repr(command)}")
		window.send_text(f'{command}\r')

		# Read the output of the command
		for attempt in attempt_n_times():
			try:
				with open(OUTPUT_CAPTURE_PATH, 'r') as f:
					return f.read()
			except FileNotFoundError:
				pass
		else:
			pytest.fail('It took too long to get the output of the command', False)

	return run

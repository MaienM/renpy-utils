# -*- coding: future_fstrings -*-

import signal
import subprocess
import time

import pytest

from constants import DEFAULT_SLEEP


def attempt_n_times(times = 20, interval = DEFAULT_SLEEP):
	"""
	Attempt an action multiple times before giving up, pausing between attempts.

	>>> for attempt in attempt_n_times():
	...   if attempt > 5:
	...     print('success')
	...     break
	... else:
	...   print('failure')
	success
	>>> for attempt in attempt_n_times():
	...   if attempt > 50:
	...     print('success')
	...     break
	... else:
	...   print('failure')
	failure
	"""
	for i in range(0, times):
		time.sleep(interval)
		yield i + 1


def popen_terminate_with_signals(popen, signals = [signal.SIGINT, signal.SIGTERM, signal.SIGKILL], timeout = 1):
	"""
	Attempt to terminate a subprocess.Popen instance with the given signals.

	Raises an Exception if the process has not terminated after all the given signals + timeouts.
	"""
	if popen.poll() is not None:
		return
	for sig in signals:
		try:
			popen.send_signal(sig)
			popen.wait(timeout)
			return
		except subprocess.TimeoutExpired:
			pass
	pytest.fail(f'Unable to stop {popen.args}, potentially leaving behind a process with pid {popen.pid}', False)

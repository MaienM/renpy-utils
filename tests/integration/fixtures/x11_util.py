# -*- coding: future_fstrings -*-

import time

import Xlib.X
import Xlib.XK
import Xlib.display
import Xlib.protocol


# Build a map for char_to_keysym
CHAR_TO_KEYSYM = {}
for var, keysym in vars(Xlib.XK).items():
	if not var.startswith('XK_'):
		continue
	char = Xlib.XK.keysym_to_string(keysym)
	if char:
		CHAR_TO_KEYSYM[char] = keysym


def char_to_keysym(char):
	"""
	The inverse of Xlib.XK.keysym_to_string.

	Based on names you might expect that this is the same as Xlib.XK.string_to_keysym, but that method only works for a
	subset of the characters. For example, ' ' or '!' will not work in that method, but it will in this one.

	Examples
	--------
	>>> Xlib.XK.keysym_to_string(char_to_keysym('a'))
	'a'
	>>> Xlib.XK.keysym_to_string(char_to_keysym('A'))
	'A'
	>>> Xlib.XK.keysym_to_string(char_to_keysym('Á'))
	'Á'
	>>> Xlib.XK.keysym_to_string(char_to_keysym(' '))
	' '
	>>> Xlib.XK.keysym_to_string(char_to_keysym('!'))
	'!'
	>>> Xlib.XK.keysym_to_string(char_to_keysym('\n'))
	'\n'
	>>> Xlib.XK.keysym_to_string(char_to_keysym('"'))
	'"'
	>>> Xlib.XK.keysym_to_string(char_to_keysym('('))
	'('
	"""
	return CHAR_TO_KEYSYM.get(char, None)


class Display(object):
	""" An X11 display. """

	def __init__(self, display):
		self.handle = display

	def get_windows(self):
		""" Get all windows for the display. """
		return self.handle.screen().root.query_tree().children

	def send_click(self, x, y, button = Xlib.X.Button1, duration = None):
		""" Simulate a mouse click at the given coordinates. """
		self.handle.screen().root.warp_pointer(x, y)
		self.handle.xtest_fake_input(Xlib.X.ButtonPress, button, x = x, y = y)
		if duration:
			time.sleep(duration)
		self.handle.xtest_fake_input(Xlib.X.ButtonRelease, button, x = x, y = y)
		self.handle.sync()


class Window(object):
	""" An X11 window. """

	def __init__(self, display, window):
		self.display = display
		self.handle = window

	def send_keyevent(self, event_type, keysym, modifiers = Xlib.X.NONE):
		""" Send a key event to the given window. """
		(keycode, offset) = list(self.display.handle.keysym_to_keycodes(keysym))[0]
		event = event_type(
			detail = keycode,
			time = Xlib.X.CurrentTime,
			root = self.display.handle.screen().root,
			window = self.handle,
			child = Xlib.X.NONE,
			root_x = 1,
			root_y = 1,
			event_x = 1,
			event_y = 1,
			state = modifiers | offset,
			same_screen = 1,
		)
		self.handle.send_event(event)
		self.display.handle.sync()

	def send_keypress(self, *args, **kwargs):
		""" Send a keypress event. """
		self.send_keyevent(Xlib.protocol.event.KeyPress, *args, **kwargs)

	def send_keyrelease(self, *args, **kwargs):
		""" Send a keyrelease event. """
		self.send_keyevent(Xlib.protocol.event.KeyRelease, *args, **kwargs)

	def send_key(self, keysym, modifiers = Xlib.X.NONE, duration = None):
		""" Send a keypress and release to a given window. """
		self.send_keypress(keysym, modifiers)
		if duration:
			time.sleep(duration)
		self.send_keyrelease(keysym, modifiers)

	def send_text(self, string, duration = None, pause = None):
		"""
		Type a string in a given window.

		"""
		# First, get the keysyms for the script
		keysyms = [char_to_keysym(c) for c in string]
		if Xlib.XK.NoSymbol in keysyms:
			unknown_characters = ''.join({string[i] for i, keysym in enumerate(keysyms) if keysym == Xlib.XK.NoSymbol})
			raise ValueError(f'Unable to convert character(s) "{unknown_characters}" to key symbols')

		for keysym in keysyms:
			self.send_key(keysym, duration = duration)
			if pause:
				time.sleep(pause)

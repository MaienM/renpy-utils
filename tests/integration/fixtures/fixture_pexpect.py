# -*- coding: future_fstrings -*-

import sys

import pexpect
import pytest
from _pytest.monkeypatch import MonkeyPatch


original_spawn = pexpect.spawn
original_run = pexpect.run


def add_kwargs(kwargs):
	""" Set some defaults for the tests on the kwargs for pexpect methods. """
	# Send log to stdout so pytest can display the full output of the subprocess if something fails
	if 'logfile' not in kwargs:
		kwargs['logfile'] = sys.stdout
	# Use UTF-8
	if 'encoding' not in kwargs:
		kwargs['encoding'] = 'UTF-8'


class SpawnExtra(original_spawn):
	""" A subclass of pexpect.spawn that adds some extra functions. """

	def __init__(self, *args, **kwargs):
		add_kwargs(kwargs)
		super(SpawnExtra, self).__init__(*args, **kwargs)

	def terminate_or_raise(self, *args, **kwargs):
		""" A variant of terminate that throws if the child is not terminated successfully. """
		if not self.terminate(*args, **kwargs):
			pytest.fail(f'Unable to stop {self.command}, potentially leaving behind a process with pid {self.pid}', False)


def run_extra(*args, **kwargs):
	add_kwargs(kwargs)
	return original_run(*args, **kwargs)


@pytest.fixture(autouse = True, scope = 'session')
def pexpect():
	monkeypatch = MonkeyPatch()
	monkeypatch.setattr('pexpect.spawn', SpawnExtra)
	monkeypatch.setattr('pexpect.run', run_extra)
	yield
	monkeypatch.undo()

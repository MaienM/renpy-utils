# -*- coding: future_fstrings -*-

import pytest

from renpy_utils.util import Bunch
import renpy_utils.parser as testmodule


@pytest.fixture
def setup_parser():
	def inner(parser):
		""" Example parser. """
		parser.add_argument('-f', '--foo', action = 'store_true')
	return inner


@pytest.fixture
def main():
	def inner(args):
		""" Example main. """
		return f'Foo: {args.foo}'
	return inner


@pytest.fixture
def module(setup_parser, main):
	return Bunch(setup_parser = setup_parser, main = main)


class TestModuleArgumentParser(object):
	def test_single(self, module):
		parser = testmodule.ModuleArgumentParser(module)
		assert parser.description.strip() == 'Example main.'
		assert parser.run(['-f']) == 'Foo: True'

	def test_single_description(self, module):
		with pytest.raises(ValueError):
			testmodule.ModuleArgumentParser(module, description = 'this is not allowed')

	def test_single_nosetup(self, main):
		module = Bunch(main = main)
		with pytest.raises(ValueError):
			testmodule.ModuleArgumentParser(module)

	def test_single_nodoc_setup(self, module):
		def setup_parser(parser):
			pass
		module.setup_parser = setup_parser
		parser = testmodule.ModuleArgumentParser(module)
		assert parser.description.strip() == 'Example main.'

	def test_single_nodoc_main(self, module):
		def main(args):
			pass
		module.main = main
		with pytest.raises(ValueError):
			testmodule.ModuleArgumentParser(module)

	def test_single_nodoc_main_with_help(self, module):
		def main(args):
			pass
		module = Bunch(module, main = main, HELP = 'Helptext')
		parser = testmodule.ModuleArgumentParser(module)
		assert parser.description.strip() == 'Helptext'

	def test_single_nomain_run(self, setup_parser):
		module = Bunch(setup_parser = setup_parser, HELP = 'Helptext', __name__ = 'samplemodule')
		parser = testmodule.ModuleArgumentParser(module)
		with pytest.raises(NotImplementedError) as e:
			parser.run([])
			assert 'samplemodule' in e.message

	class TestAddSubparsers(object):
		def test_nested(self, capsys, setup_parser):
			def setup_subparser(parser):
				""" Example subparser. """
				setup_parser(parser)
			def main(args):
				""" Example main. """
				return 'Foo'
			def submain(args):
				""" Example submain. """
				return 'Bar'
			module = Bunch(setup_parser = setup_parser, main = main)
			submodule = Bunch(setup_parser = setup_subparser, main = submain)
			parser = testmodule.ModuleArgumentParser(module)
			parser.add_subparsers(dest = 'sub', modules = { 'sub1': submodule }, required = False)

			assert parser.run(['-f']) == 'Foo'
			with pytest.raises(SystemExit):
				parser.run(['-h'])
			output = capsys.readouterr().out
			assert 'Example main' in output
			assert 'Example submain' in output
			assert 'sub1' in output

			assert parser.run(['sub1', '-f']) == 'Bar'
			with pytest.raises(SystemExit):
				parser.run(['sub1', '-h'])
			output = capsys.readouterr().out
			assert 'Example main' not in output
			assert 'Example submain' in output

		def test_nested_required(self, capsys, setup_parser):
			def setup_subparser(parser):
				""" Example subparser. """
				setup_parser(parser)
			def main(args):
				""" Example main. """
				return 'Foo'
			def submain(args):
				""" Example submain. """
				return 'Bar'
			module = Bunch(setup_parser = setup_parser, main = main)
			submodule = Bunch(setup_parser = setup_subparser, main = submain)
			parser = testmodule.ModuleArgumentParser(module)
			parser.add_subparsers(dest = 'sub', modules = { 'sub1': submodule }, required = True)

			with pytest.raises(SystemExit):
				parser.run(['-f'])
			assert parser.run(['sub1', '-f']) == 'Bar'

		def test_no_modules(self, capsys, setup_parser):
			def setup_subparser(parser):
				""" Example subparser. """
				setup_parser(parser)
			def main(args):
				""" Example main. """
				return 'Foo'
			module = Bunch(setup_parser = setup_parser, main = main)
			parser = testmodule.ModuleArgumentParser(module)
			with pytest.raises(ValueError):
				parser.add_subparsers(dest = 'sub')
			with pytest.raises(ValueError):
				parser.add_subparsers(dest = 'sub', modules = {})

from collections import OrderedDict

import pytest

from renpy_utils.util import Bunch
import renpy_utils.json_converter as testmodule


class Foo(object):
	pass


class Bar(object):
	pass


class FooBar(Foo, Bar):
	pass


class NewSet(set):
	pass


@pytest.fixture
def converter():
	return testmodule.Converter()


class TestConversionFailure(object):
	def test(self):
		failure = testmodule.ConversionFailure(kind = 'thing', context = 'foo.bar', value = True)
		message = str(failure)
		assert 'thing' in message
		assert 'foo.bar' in message
		assert 'True' in message
		assert 'bool' in message
		assert 'ConversionFailure' not in message
		rep_message = repr(failure)
		assert 'ConversionFailure' in rep_message
		assert message in rep_message


class TestConverter(object):
	def test_types(self, converter):
		assert converter.convert('foo') == 'foo'
		assert converter.convert(12) == 12
		assert converter.convert(12.34) == 12.34
		assert converter.convert(True) is True
		assert converter.convert(False) is False
		assert converter.convert(None) is None
		assert converter.failures == []

	def test_extended_types(self, converter):
		assert converter.convert(b'foo') == 'foo'
		assert converter.failures == []

	def test_iterable(self, converter):
		assert converter.convert([1, 2, 3]) == [1, 2, 3]
		assert converter.convert(set([1, 2, 3])) == [1, 2, 3]
		assert converter.convert((1, 2, 3)) == [1, 2, 3]
		assert converter.failures == []

	def test_dict(self, converter):
		data = { 'foo': 1, 'bar': 'baz' }
		assert converter.convert(data) == data
		assert converter.failures == []

	def test_fallback_iterable(self, converter):
		def test():
			yield 1
			yield 2
		assert converter.convert(test()) == ('Iterable extracted from object of type generator', [1, 2])

	def test_fallback_dict(self, converter):
		assert converter.convert(Bunch(foo = 1, bar = 2)) == (
			'Dict extracted from object of type Bunch',
			{ 'foo': 1, 'bar': 2 },
		)

	def test_unsupported_type(self, converter):
		with pytest.raises(TypeError):
			assert converter.convert(object())

	def test_unsupported_type_in_iterable(self, converter):
		output = converter.convert([1, object(), 2])
		assert converter.failures != []
		assert '[1]' in str(converter.failures)
		assert output == [1, 2]

	def test_unsupported_type_in_dict_key(self, converter):
		output = converter.convert({ 'foo': 1, object(): 2, 'baz': 3 })
		assert converter.failures != []
		assert 'object' in str(converter.failures)
		assert output == { 'foo': 1, 'baz': 3 }

	def test_unsupported_type_in_dict_value(self, converter):
		output = converter.convert({ 'foo': 1, 'bar': object(), 'baz': 3 })
		assert converter.failures != []
		assert 'bar' in str(converter.failures)
		assert 'object' in str(converter.failures)
		assert output == { 'foo': 1, 'baz': 3 }

	def test_custom_type(self):
		converter = testmodule.Converter({ Foo: lambda *a: 'Foo thing!' })
		output = converter.convert(Foo())
		assert converter.failures == []
		assert output == 'Foo thing!'

	def test_custom_subtype(self):
		converter = testmodule.Converter({ Foo: lambda *a: 'Foo thing!' })
		output = converter.convert(Foo())
		assert converter.failures == []
		assert output == 'Foo thing!'

	def test_custom_subtype_multi_inheritance(self):
		converters = OrderedDict()
		converters[Foo] = lambda *a: 'Foo thing!'
		converters[Bar] = lambda *a: 'Bar thing!'
		converter = testmodule.Converter(converters)
		print(list(converter.type_mappings.keys()))
		output = converter.convert(FooBar())
		assert converter.failures == []
		assert output == 'Foo thing!'

	def test_subtype_of_base_type(self, converter):
		output = converter.convert(NewSet([1, 2, 3]))
		assert converter.failures == []
		assert output == [1, 2, 3]

	def test_custom_subtype_broken(self):
		converter = testmodule.Converter({ Foo: lambda v, *a: v + 1 })
		converter.convert(Foo())
		assert converter.failures != []

	def test_custom_fallback_broken(self):
		converter = testmodule.Converter(fallbacks = [lambda v, *a: v + 1])
		converter.convert(Foo())
		assert converter.failures != []

import io
import os
import os.path
import pickle
import zipfile

import pytest

from renpy_utils.util import Bunch


SAVE_DIR = os.path.join(os.path.dirname(__file__), '..', 'saves', 'saves')


@pytest.fixture(params = os.listdir(SAVE_DIR))
def sample_save(request):
	""" Provide a real save file from a random game. """
	with open(os.path.join(SAVE_DIR, request.param), 'rb') as save:
		yield save


@pytest.fixture
def mock_save(tmpdir):
	""" Provide a fake savefile with known data. """
	data = {
		'foo': 1,
		'bar': 'hello',
		b'baz': {1, 2, 3},
		'foobar': Bunch(bazfoo = 23, bazbar = 'lorem'),
	}
	with io.BytesIO() as save:
		with zipfile.ZipFile(save, 'w') as zip:
			logfile = tmpdir.join('log')
			with logfile.open('wb') as log:
				pickle.dump((data,), log)
			zip.write(logfile.strpath, 'log')
		yield Bunch(data = data, save = save)

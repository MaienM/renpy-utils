# -*- coding: future_fstrings -*-

from unittest.mock import MagicMock
try:  # pragma: no cover
	import __builtin__ as builtins
except ImportError:
	import builtins

import pytest

import renpy_utils.util as testmodule


class TestBunch(object):
	def test(self):
		obj = testmodule.Bunch(foo = 1)
		assert obj.foo == 1
		obj.foo = 2
		assert obj.foo == 2

	def test_attribute_error(self):
		obj = testmodule.Bunch(foo = 1)
		with pytest.raises(AttributeError):
			assert obj.bar

	def test_forbid_add_attribute(self):
		obj = testmodule.Bunch()
		with pytest.raises(AttributeError):
			obj.bar = 2

	def test_base(self):
		obj1 = testmodule.Bunch(foo = 1, bar = 2)
		obj2 = testmodule.Bunch(obj1, bar = 3)
		assert obj2.foo == 1
		assert obj2.bar == 3


class TestFormatDocstring(object):
	def test_func(self):
		@testmodule.format_docstring(the_answer = 42)
		def test():
			""" The answer is {the_answer} """
		assert test.__doc__.strip() == 'The answer is 42'

	def test_class(self):
		@testmodule.format_docstring(the_answer = 42)
		class Test(object):
			""" The answer is {the_answer} """
		assert Test.__doc__.strip() == 'The answer is 42'

	def test_no_docstring(self):
		@testmodule.format_docstring(the_answer = 42)
		def test():
			pass
		assert test.__doc__ is None


class TestGetSections(object):
	def test_no_sections(self):
		def test():
			"""
			Hello world

			Lorem Ipsum
			"""
		assert testmodule.get_sections(test.__doc__) == { '': 'Hello world\n\nLorem Ipsum' }

	def test_single_section(self):
		def test():
			"""
			Hello world

			Examples
			--------
			Lorem Ipsum

			Dolor Sit Amet
			"""
		assert testmodule.get_sections(test.__doc__) == {
			'': 'Hello world\n',
			'Examples': 'Lorem Ipsum\n\nDolor Sit Amet',
		}

	def test_multi_section(self):
		def test():
			"""
			Hello world

			Foo
			---
			Lorem Ipsum

			Bar
			---
			Dolor Sit Amet
			"""
		assert testmodule.get_sections(test.__doc__) == {
			'': 'Hello world\n',
			'Foo': 'Lorem Ipsum\n',
			'Bar': 'Dolor Sit Amet',
		}

	def test_section_repeat(self):
		def test():
			"""
			Hello world

			Foo
			---
			Lorem Ipsum

			Foo
			---
			Dolor Sit Amet
			"""
		assert testmodule.get_sections(test.__doc__) == {
			'': 'Hello world\n',
			'Foo': 'Dolor Sit Amet',
		}

	def test_section_invalid_header(self):
		def test():
			"""
			Hello world

			Foo
			-----
			Lorem Ipsum
			"""
		assert testmodule.get_sections(test.__doc__) == { '': 'Hello world\n\nFoo\n-----\nLorem Ipsum' }


class TestTruncate(object):
	def test_string(self):
		assert testmodule.truncate('Lorem ipsum dolor sit amet', 100, '...') == 'Lorem ipsum dolor sit amet'
		assert testmodule.truncate('Lorem ipsum dolor sit amet', 25, '...') == 'Lorem ipsum dolor sit ...'

	def test_list(self):
		assert testmodule.truncate(list(range(1, 100)), 5) == [1, 2, 3, 4, 5]
		assert testmodule.truncate(list(range(1, 100)), 5, ['and more']) == [1, 2, 3, 4, 'and more']


class TestSafeStr(object):
	@pytest.fixture
	def obj(self):
		mock = MagicMock()
		mock.__str__.return_value = 'result of __str__'
		mock.__repr__ = MagicMock()
		mock.__repr__.return_value = 'result of __repr__'
		return mock

	def break_method(self, mock, method):
		getattr(mock, method).side_effect = Exception('This method is intentionally broken')

	def test_str(self, obj):
		assert testmodule.safe_str(obj) == 'result of __str__'

	def test_repr(self, obj):
		self.break_method(obj, '__str__')
		with pytest.raises(Exception):
			print(str(obj))
		assert testmodule.safe_str(obj) == 'result of __repr__'

	def test_object_repr(self, obj):
		self.break_method(obj, '__str__')
		self.break_method(obj, '__repr__')
		with pytest.raises(Exception):
			print(repr(obj))
		result = testmodule.safe_str(obj)
		assert 'MagicMock' in result
		assert '0x' in result


class TestMonkey(object):
	def test_rewrite_function(self):
		def rewritten(*args, **kwargs):
			return rewritten.original(*args, **kwargs) + 10
		assert sum([1, 2]) == 3
		with testmodule.Monkey() as m:
			rewritten.original = sum
			m.setattr(builtins, 'sum', rewritten)
			assert sum([1, 2]) == 13
		assert sum([1, 2]) == 3

	def test_setattr(self):
		def testfunc():
			pass
		assert testfunc.__name__ == 'testfunc'
		with testmodule.Monkey() as m:
			m.setattr(testfunc, '__name__', 'bar')
			assert testfunc.__name__ == 'bar'
		assert testfunc.__name__ == 'testfunc'

	def test_setattr_new(self):
		def testfunc():
			pass
		assert not hasattr(testfunc, 'stuff')
		with testmodule.Monkey() as m:
			m.setattr(testfunc, 'stuff', 'bar')
			assert testfunc.stuff == 'bar'
		assert not hasattr(testfunc, 'stuff')

	def test_setitem(self):
		data = { 'foo': 12 }
		assert data['foo'] == 12
		with testmodule.Monkey() as m:
			m.setitem(data, 'foo', 'bar')
			assert data['foo'] == 'bar'
		assert data['foo'] == 12

	def test_setitem_new(self):
		data = {}
		assert 'foo' not in data
		with testmodule.Monkey() as m:
			m.setitem(data, 'foo', 'bar')
			assert data['foo'] == 'bar'
		assert 'foo' not in data

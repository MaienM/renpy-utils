import io
import json

from renpy_utils.parser import ModuleArgumentParser
from renpy_utils.util import Bunch
import renpy_utils.saves.dump as testmodule


class TestParser(object):
	def test(self):
		ModuleArgumentParser(testmodule)


class TestMain(object):
	def test_mock(self, mock_save):
		with io.StringIO() as output:
			args = Bunch(
				save = mock_save.save,
				output = output,
				log = None,
			)
			testmodule.main(args)
			output = output.getvalue()
		assert json.loads(output)

	def test_sample(self, sample_save):
		with io.StringIO() as output, io.StringIO() as log:
			args = Bunch(
				save = sample_save,
				output = output,
				log = log,
			)
			testmodule.main(args)
			output = output.getvalue()
			log = log.getvalue()
		assert json.loads(output)
		assert log != ''

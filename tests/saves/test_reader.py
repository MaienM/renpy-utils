import pytest

import renpy_utils.saves.reader as testmodule


@pytest.fixture
def reader():
	return testmodule.SaveReader()


class TestSaveReader(object):
	def test_read_mock(self, mock_save, reader):
		output = reader.read(mock_save.save)
		assert type(output['foobar']) == type(mock_save.data['foobar'])  # noqa
		assert vars(output['foobar']) == vars(mock_save.data['foobar'])
		output['foobar'] = mock_save.data['foobar']
		assert output == mock_save.data

	def test_read_sample(self, reader, sample_save):
		data = reader.read(sample_save)
		assert data is not None
		assert data != {}

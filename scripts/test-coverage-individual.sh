#!/usr/bin/env bash

set -o errexit

GENERATED="tests/test_none.py"

rm -rf .coverage
mapfile -t files < <(git ls-files src | grep '\.py$' | grep -v __)
for file in "${files[@]}"; do
	# Get the name of the module being tested
	module="$(echo "$file" | sed 's!^src/!!; s!\.py$!!' | tr '/' '.')"

	# Generate a test file, to prevent having an error that no tests ran when testfile doesn't exit/doesn't contain any
	# tests.
	(
		echo "try:"
		echo "  import $module"
		echo "except BaseException:"
		echo "  pass"
		echo
		echo "def test_none():"
		echo "  pass"
	) > "$GENERATED"

	# Get the test files to run
	testfiles=("$GENERATED")
	testfile="$(echo "$file" | sed 's!^src/renpy_utils/!tests/!; s!/\([^/]*\.py\)$!/test_\1!')"
	[ -f "$testfile" ] && testfiles=("${testfiles[@]}" "$testfile")

	set -x
	pytest "${testfiles[@]}" -c /dev/null --cov="$module" --cov-append
	set +x
done
rm "$GENERATED"

#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import setuptools


setuptools.setup(
	name = 'renpy-utils',
	version = '0.1.0',
	license = 'MIT',
	description = "Utility functions for Ren'Py",
	author = 'Michon van Dooren',
	author_email = 'michon1992@gmail.com',
	url = 'https://gitlab.com/maienm/renpy-utils',
	classifiers = [
		'Development Status :: 3 - Alpha'
		'Intended Audience :: Developers',
		'License :: OSI Approved :: MIT License',
		'Operating System :: Unix',
		'Operating System :: POSIX',
		'Operating System :: Microsoft :: Windows',
		'Programming Language :: Python',
		'Programming Language :: Python :: 2.7',
		'Programming Language :: Python :: 3',
		'Programming Language :: Python :: 3.3',
		'Programming Language :: Python :: 3.4',
		'Programming Language :: Python :: 3.5',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: Implementation :: CPython',
		'Programming Language :: Python :: Implementation :: PyPy',
		'Topic :: Utilities',
	],
	keywords = [
		'renpy',
	],

	packages = setuptools.find_packages('src'),
	package_dir = { '': 'src' },
	include_package_data = True,
	zip_safe = False,
	install_requires = [
		'future-fstrings',
		'stickytape',
		'tokenize_rt',
	],
)

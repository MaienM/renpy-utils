"""
The main entrypoint of the command line interface for the savegame utils.

This imports the different modules, and provides a combined interface for them.
"""

import renpy_utils.saves.dump as dump


SUBMODULES = {
	'dump': dump,
}


HELP = "A set of savegame utilities for RenPy."


def setup_parser(parser):
	""" Configure the parser for this module. """
	parser.add_subparsers(
		description = 'The action to perform on the savegames.',
		dest = 'action',
		metavar = 'action',
		modules = SUBMODULES,
	)

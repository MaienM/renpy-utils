""" The dump action of the saves utilities. """

import argparse
import json

from renpy_utils.json_converter import Converter
from renpy_utils.saves.reader import SaveReader


def setup_parser(parser):
	""" Configure the parser for this module. """
	parser.add_argument(
		'save',
		help = "The location of the RenPy save file.",
		type = argparse.FileType('rb'),
	)
	parser.add_argument(
		'output',
		help = 'Location for the JSON output.',
		type = argparse.FileType('w', encoding = 'UTF-8'),
	)
	parser.add_argument(
		'-l',
		'--log',
		help = 'Location for the log describing what was lost in conversion.',
		type = argparse.FileType('w', encoding = 'UTF-8'),
		nargs = '?',
	)


def main(args):
	"""
	Dump the primary contents of a save file to JSON.

	This takes a save file, extracts the main data from it, and attempts to read this, attempts to convert types into
	JSON-compatible ones, and outputs it as JSON.
	"""
	# Read the data from the save
	reader = SaveReader()
	data = reader.read(args.save)

	# Convert data into an object fit for JSON serialization
	converter = Converter()
	data = converter.convert(data)

	# Output the results
	if args.log:
		args.log.write('\n'.join([str(failure) for failure in converter.failures]))
	json.dump(data, args.output, indent = '\t')

"""
Classes needed to read RenPy save files.

RenPy save files are zips, with a few files contained within. The main file we're interested in is called 'log', and it
is a pickled file containing the actual save data.

We unpickle this using picklemagic, which is a forgiving pickle library that has facilities to handle unpickling
nonexistant types, either by using builtin classes or by using another specified class. This module contains a lot of
classes for this purpose.
"""

import zipfile

import renpy_utils.vendor.picklemagic.picklemagic as picklemagic


__all__ = ['stub', 'SaveReader']


def stub(module, name):
	""" Create a copy of a class with a specific name, for use with picklemagic. """
	def decorator(klass):
		class Klass(klass, picklemagic.FakeStrict):
			pass
		Klass.__module__ = module or klass.__module__
		Klass.__name__ = name or klass.__name__
		return Klass
	return decorator


@stub('renpy.python', None)
class RevertableDict(dict):
	pass


@stub('renpy.python', None)
class RevertableList(list):
	pass


@stub('renpy.python', None)
class RevertableSet(set):  # pragma: no cover
	def __setstate__(self, state):
		if isinstance(state, tuple):
			self.update(state[0].keys())
		else:
			self.update(state)

	def __getstate__(self):
		return ({ i: True for i in self },)

	__reduce__ = object.__reduce__
	__reduce_ex__ = object.__reduce_ex__


class SaveReader(object):
	""" Class to manage reading RenPy save files. """

	STUB_CLASSES = [
		RevertableDict,
		RevertableList,
		RevertableSet,
		set,
	]

	def __init__(self, classes = []):
		"""
		Create a new instance.

		Parameters
		----------
		classes : list
			Extra classes that should be available while reading the save.

			Make sure these have the right name/module (see the picklemagic docs for more info). This module has a
			decorator (stub) that can help with this.
		"""
		# Build a factory out of the stubbed classes
		self.classes = classes + self.STUB_CLASSES
		self.factory = picklemagic.FakeClassFactory(self.classes, picklemagic.FakeIgnore)

	def read(self, file):
		"""
		Read a zipfile.

		Parameters
		----------
		file : file
			Anything that is a valid 'file' parameter for zipfile.Zipfile.

		Returns
		-------
		dict
			The unpickled data.

			This is a dictionary, where the values and their types depend on what was in the save, which might differ
			drastically from game to game.
		"""
		# Open the savefile and read the pickled data from it
		with zipfile.ZipFile(file, 'r') as zip:
			with zip.open('log', 'r') as f:
				data = f.read()

		# Unpickle the save data
		result = picklemagic.safe_loads(data, class_factory = self.factory)

		# Get the actual data (second element is rollback history, which we're not interested in for now)
		result = result[0]

		return result

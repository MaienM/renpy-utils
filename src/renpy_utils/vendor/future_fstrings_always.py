""" A wrapper around future-fstrings that will work in versions with native fstring support. """

import importlib
try:  # pragma: no cover
	import __builtin__ as builtins
except ImportError:
	import builtins

import future_fstrings
import tokenize_rt

from renpy_utils.util import Monkey


# future-fstrings will disable itself in versions that natively support fstrings. This won't do for our builds, as these
# are run in a different environmrent. To counteract this we make the check always fail, so it will remain functional
# even in versions with native fstring support.
with Monkey() as m:
	m.setattr(builtins, 'eval', lambda s: False)
	importlib.reload(future_fstrings)


def src_to_tokens(data):
	"""
	Transform source code to tokens.

	This is a wrapper around tokenize_rt.src_to_tokens that will return the pre-3.6 tokens for fstrings, even on 3.6 and
	above.
	"""
	tokens = src_to_tokens.original(data)

	# Before fstring support an fstring will look like a NAME followed by a STRING. With fstring support it will look
	# like just a STRING. Split these back up into two tokens.
	to_split = []
	for i, token in enumerate(tokens):
		if token.name == 'STRING' and token.src[0] == 'f':
			to_split.append(i)
	for i in reversed(to_split):
		token = tokens[i]
		tokens[i:i + 1] = [
			tokenize_rt.Token(
				name = 'NAME',
				src = 'f',
				line = token.line,
				utf8_byte_offset = token.utf8_byte_offset,
			),
			tokenize_rt.Token(
				name = 'STRING',
				src = token.src[1:],
				line = token.line,
				utf8_byte_offset = token.utf8_byte_offset + 1,
			),
		]

	# If the code starts with a future_fstrings encoding, switch this to utf-8, as there is no need to apply the
	# transform multiple times.
	if tokens[0].name == 'COMMENT' and 'coding' in tokens[0].src:
		tokens[0] = tokenize_rt.Token(
			name = 'COMMENT',
			src = tokens[0].src.replace('future-fstrings', 'utf-8').replace('future_fstrings', 'utf-8'),
			line = tokens[0].line,
			utf8_byte_offset = tokens[0].utf8_byte_offset,
		)

	return tokens


def decode(*args, **kwargs):
	"""
	Read a source file.

	Like future_fstrings.decode, but will transform fstrings even if the current python version natively supports them.
	"""
	with Monkey() as m:
		src_to_tokens.original = tokenize_rt.src_to_tokens
		m.setattr(tokenize_rt, 'src_to_tokens', src_to_tokens)
		return future_fstrings.decode(*args, **kwargs)

# -*- coding: future_fstrings -*-

""" The build action of the runtime components. """

import argparse
import io
import os.path
import re
try:  # pragma: no cover
	import __builtin__ as builtins
except ImportError:
	import builtins

import stickytape

import renpy_utils
from renpy_utils.util import Monkey
import renpy_utils.vendor.future_fstrings_always as future_fstrings


ROOTDIR = os.path.realpath(os.path.join(os.path.dirname(renpy_utils.__file__), '..'))
DIRNAME = os.path.realpath(os.path.join(os.path.dirname(__file__), 'sources'))
ENTRYPOINT = os.path.join(DIRNAME, 'entrypoint.rpy')
RE_INLINE = re.compile(r'^\n*(?P<indent>\s*)(?P<filler>.*?){{{(?P<path>.*?)}}}', re.MULTILINE)


class InliningError(Exception):
	""" An error while inlining data into an .rpy file. """


def setup_parser(parser):
	""" Configure the parser for this module. """
	parser.add_argument('out', type = argparse.FileType('w', encoding = 'UTF-8'))


def build_py(file):
	""" Combine a python files + its dependencies into a single python code blob. """
	# Rewrite open while running stickytape to apply the future-fstrings transform.
	real_open = open
	def fake_open(path, mode = 'r', *args, **kwargs):
		if not (path.endswith('.py') and os.path.realpath(path).startswith(ROOTDIR) and 'r' in mode):
			return real_open(path, mode, *args, **kwargs)

		with real_open(path, 'rb') as f:
			data, _ = future_fstrings.decode(f.read())

		return io.StringIO(data)

	# Combine the python files
	with Monkey() as m:
		m.setattr(builtins, 'open', fake_open)
		script = stickytape.script(file, [ROOTDIR])

	# Stickytape uses os.mkdir to create directories, but doesn't account for the parent directories not existing yet
	# (due to nesting/order). Rewrite these to makedirs to prevent this issue
	script = script.replace('os.mkdir', 'os.makedirs')

	return script


def build_rpy(path, inlined = None):
	"""
	Compile a .rpy template.

	This is a regular .rpy file, with a single addition: any token in the form of {{{filename}}} will be inlined.

	- An inlined .py file will also inline any required dependencies.
	- An inlined .rpy will be compiled with this function.
	- Any other filetype will be inlined as a single line bytestring.

	If the result of any of these is multiline, all lines will be indented by the same amount as the line containing the
	token.
	"""
	if inlined is None:
		inlined = []

	def inline(m):
		inline_path_raw = m.group('path')
		inline_path = os.path.join(os.path.dirname(path), inline_path_raw)
		indent = m.group('indent')
		filler = m.group('filler')

		try:
			if inline_path.endswith('.rpy'):
				if inline_path in inlined:
					data = f'# {inline_path_raw} already inlined'
				else:
					inlined.append(inline_path)
					data = build_rpy(inline_path, inlined)

			elif inline_path.endswith('.py'):
				data = build_py(inline_path)

			else:
				with open(inline_path, 'rb') as f:
					data = repr(f.read())
		except IOError as e:
			raise InliningError(f'Unable to inline {inline_path_raw} from {path} (resolved to {inline_path})', e)

		# Handle multiline results
		data = data.replace('\n', f'\n{indent}')

		return f'{indent}{filler}{data}'

	# Read the template
	with open(path, 'r') as f:
		script = f.read()

	# Process all inlining tokens
	script = RE_INLINE.sub(inline, script)

	# Ren'py doesn't accept tabs as indents, so replace these with spaces
	script = script.replace('\t', '    ')

	return script


def main(args):
	"""
	Build the runtime utilities into an .rpy file.

	This takes the files from the sources directory and combines them in such a way that it becomes a valid .rpy file. This
	file can then be dropped in the game directory of a RenPy game to make the runtime components available in this game.
	"""
	# Build the entrypoint template
	script = build_rpy(ENTRYPOINT)

	# Write to the output file
	args.out.write(script)

"""
The main entrypoint of the command line interface for the runtime components.

This imports the different modules, and provides a combined interface for them.
"""

import renpy_utils.runtime.build as build
import renpy_utils.runtime.install as install


SUBMODULES = {
	'build': build,
	'install': install,
}


HELP = "A set of runtime utilities for RenPy, available through the in-game console."


def setup_parser(parser):
	""" Configure the parser for this module. """
	parser.add_subparsers(
		description = 'The action to perform on the runtime components.',
		dest = 'action',
		metavar = 'action',
		modules = SUBMODULES,
	)

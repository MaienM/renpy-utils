init 3000 python:
	{{{ui.py}}}

	renpy.define_screen('renpy_utils_ui_base', renpy_utils_ui_base, modal = 'True')
	renpy.define_screen('renpy_utils_screen', renpy_utils_screen, modal = 'True')
	renpy.define_screen('renpy_utils_confirm', renpy_utils_confirm, modal = 'True')
	renpy.define_screen('renpy_utils_message', renpy_utils_message, modal = 'True')

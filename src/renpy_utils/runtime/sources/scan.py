# -*- coding: future_fstrings -*-

""" Methods related to scanning the variables to inspect the state and to find out what can be modified. """


class ScanMatch(object):
	"""
	A matching variable found by scan.

	Attributes
	----------
	key : string
		The name of the found variable.
	value
		The value of the found variable.
	context : list of string
		The names of the parent(s) of the object the variable was contained in. For a top level result this will be empty.
	"""

	def __init__(self, key, value, context = None):
		""" Create a new ScanMatch. """
		self.key = key
		self.value = value
		self.context = [] if context is None else context

	def __repr__(self):
		""" Return a string representation of the match that contains all info needed to reconstruct it. """
		return f'ScanMatch(key={self.key!r}, value={self.value!r}, context={self.context!r})'

	@property
	def path(self):
		""" Get the full path of the matching variable. """
		return '.'.join(self.context + [self.key])


def scan(filter, data, recurse = 0):
	"""
	Scan the given object for variables matching the given filter.

	Examples
	--------
	>>> data = { 'foo': 1, 'bar': 20, 'baz': { 'foo': 12 } }
	>>> list(scan(lambda k, v: k[0] == 'f', data))
	[ScanMatch(key='foo', value=1, context=[])]
	>>> list(scan(lambda k, v: v > 10, data))
	[ScanMatch(key='bar', value=20, context=[])]
	>>> sorted(scan(lambda k, v: v > 10, data, 1), key=lambda m: m.path)
	[ScanMatch(key='bar', value=20, context=[]), ScanMatch(key='foo', value=12, context=['baz'])]
	>>> list(scan(lambda k, v: v > 10, [12, 2, 4, 6, 20]))
	[ScanMatch(key='0', value=12, context=[]), ScanMatch(key='4', value=20, context=[])]

	Parameters
	----------
	filter : func
		A function that receives two arguments: the key and value. If it returns true, the variable will be in the
		resulting object.
	data
		The item to filter.
	recurse : int
		The amount of levels to recurse.

	Returns
	-------
	list of ScanMatch
		The found matches.
	"""
	# Convert the data to a set of key-value pairs to scan through
	if hasattr(data, '__dict__'):
		data = data.__dict__.items()
	elif isinstance(data, dict):
		data = data.items()
	else:
		data = enumerate(data)

	# Scan through the found set
	for (k, v) in data:
		# See if the item itself should be included
		try:
			if filter(k, v):
				yield ScanMatch(str(k), v)
		except Exception:
			pass

		# Recursive if needed and possible
		if recurse > 0:
			try:
				for subresult in scan(filter, v, recurse - 1):
					subresult.context.insert(0, str(k))
					yield subresult
			except TypeError:
				pass

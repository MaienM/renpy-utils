""" Generic UI related functions. """

# Make sure renpy and ui are available
if 'renpy' not in dir():
	renpy = None
	raise Exception("renpy should be available from RenPy, but it is missing")
if 'ui' not in dir():
	ui = None
	raise Exception("ui should be available from RenPy, but it is missing")


def args_by_version(*data_by_version):
	"""
	Build a dictionary of arguments depending on the renpy version.

	Examples
	--------
	>>> from renpy_utils.util import Bunch
	>>> input = (
	...   ((0,), dict(foo = 1, bar = 1, baz = 1)),
	...   ((5, 8), dict(foo = 2, bar = 2)),
	...   ((5, 11), dict(foo = 3, baz = 2)),
	... )
	>>> renpy = Bunch(version_tuple = (5, 6, 1, 2, 3))
	>>> args_by_version(**input)
	{'foo': 1, 'bar': 1, 'baz': 1}
	>>> renpy = Bunch(version_tuple = (5, 10, 1, 2, 3))
	>>> args_by_version(**input)
	{'foo': 2, 'bar': 2, 'baz': 1}
	>>> renpy = Bunch(version_tuple = (5, 10, 1, 2, 3))
	>>> args_by_version(**input)
	{'foo': 3, 'bar': 2, 'baz': 3}
	"""
	result = {}
	for min_version, data in data_by_version:
		if renpy.version_tuple >= min_version:
			result.update(data)
	return result


class RenpyUtilsButton(object):
	""" Contains information for a button to be displayed in the dialog. """

	def __init__(self, label, description = None, action = None):
		""" Create a new RenpyUtilsButton object. """
		self.label = label
		self.description = description
		self.action = action


def renpy_utils_tooltip(_scope = None, **kwargs):
	""" Setups a tooltip. """
	if not hasattr(_scope, 'tooltip'):
		_scope.tooltip = Tooltip('Choose an action below.')
	return _scope.tooltip


def renpy_utils_ui_base(title = 'Untitled', **kwargs):
	""" Render a base frame. """
	ui.key('K_ESCAPE', action = ui.returns(None))

	ui.frame(**args_by_version(
		((0,), dict(
			xfill = True,
			align = (0.5, 0.5),
		)),
		((5, 99, 11), dict(
			margin = (50, 50),
			padding = (20, 20),
		)),
	))
	ui.vbox(spacing = 10)

	ui.text(title, size = 30)


def renpy_utils_screen(buttons = [], _scope = None, **kwargs):
	""" Render a screen with a list of choices. """
	renpy_utils_ui_base(**kwargs)
	renpy_utils_tooltip(_scope = _scope)

	ui.text(_scope.tooltip.value, yoffset = 5)

	for i, button in enumerate(buttons):
		ui.textbutton(
			button.label,
			action = button.action or ui.returns(i),
			hovered = _scope.tooltip.Action(button.description),
		)


def renpy_utils_confirm(title = '', description = '', action = ui.returns(True), **kwargs):
	""" Render a confirmation prompt. """
	renpy_utils_ui_base(title = title, **kwargs)

	ui.text(description, yoffset = 5)
	ui.textbutton(title, action = ui.returns(True))
	ui.textbutton('Cancel', action = ui.returns(False))


def renpy_utils_message(description = '', **kwargs):
	""" Render an informational dialog. """
	renpy_utils_ui_base(**kwargs)

	ui.text(description, yoffset = 5)
	ui.textbutton('Ok', action = ui.returns(None))

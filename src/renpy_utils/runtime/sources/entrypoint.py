"""
The entrypoint of the RenPy runtime components.

This file will be compiled to a single file, inlining all its dependencies, and it will then be put in the
entrypoint.rpy template to result in a self-contained .rpy file that can be dropped in the game folder of a RenPy game
to install.

This file, as well as any modules used by it (directly or otherwise) should work in python 2.7, as this is what RenPy
uses.
"""


from renpy_utils.runtime.sources.command import init

# Make sure _console is available
if '_console' not in dir():
	_console = None
	raise Exception("_console should be available from RenPy, but it is missing")

# Force enable the console
_console.console = _console.DebugConsole()
_console.config.console = True

# Force enable rollback
_console.config.rollback_enabled = True
_console.config.rollback_length = 256
_console.config.hard_rollback_limit = _console.config.rollback_length

# Add custom actions available for keybindings
keymap = _console.renpy.Keymap(
	renpy_utils_menu = _console.ui.callsinnewcontext('renpy_utils_menu'),
)
_console.config.underlay.append(keymap)

# (Re-)create keybindings
_console.config.keymap['console'].append('alt_K_BACKQUOTE')
_console.config.keymap['game_menu'].append('mouseup_3')
_console.config.keymap['renpy_utils_menu'] = ['alt_shift_K_u']

# Add commands
init(_console)

{{{ui.rpy}}}

label renpy_utils_menu:
	call screen renpy_utils_screen(
		title = "RenPy Utils Menu",
		buttons = [
			RenpyUtilsButton(
				label = 'Clear console history',
				description = (
					'This will clear the history of the development console. '
					'This can be useful when the contents of the history prevent you from opening the console.'
				),
				action = ui.callsinnewcontext('renpy_utils_menu_clear_history'),
			),
			RenpyUtilsButton(
				label = 'Close',
				description = 'Close this menu.',
			),
		],
	)
	return

label renpy_utils_menu_clear_history:
	call screen renpy_utils_confirm(
		title = 'Clear history',
		description = 'This will clear the history of the development console. This is irreversible.',
	)
	if not _return:
		return
	$ del _console.console.history[:]
	call screen renpy_utils_message(
		title = 'History cleared',
		description = 'The console history has been successfully been cleared.',
	)
	return

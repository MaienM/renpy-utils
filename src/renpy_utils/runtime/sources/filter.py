# -*- coding: future_fstrings -*-

"""
Functions related to building filter methods that can be used to filter a list.

Each of these build_filter functions will be called with a single argument: the query that was input by the user. This
query is always a string. Based on this query, a filter method will be built, if the builder can do so for the given
query.

If the filter was successfully built, it will be a function that accepts one arguments: the value to filter. This can be
of any type.
"""

import functools
import re

from renpy_utils.util import safe_str

# Only needed in python 3.x
try:
	from functools import reduce
except ImportError:  # pragma: no cover
	pass


def builder(pattern):
	"""
	Given a pattern, create a decorator to wrap filter builder methods in.

	If the filter build method is called with a query that does not match the given pattern, nothing will be returned.

	Examples
	--------
	>>> @builder('^(?P<input>[a-z]+)$')
	... def build_test(input=''):
	...   return lambda q: q == input
	>>> build_test('foo')('foo')
	True
	>>> build_test('foo')('bar')
	False
	>>> build_test('123')
	"""
	pattern = re.compile(pattern)
	def decorator(func):
		@functools.wraps(func)
		def wrapper(query):
			match = pattern.match(query)
			if match:
				filter = func(**match.groupdict())
				filter.builder = wrapper
				return filter
		return wrapper
	return decorator


REGEX_FLAGS = {
	'i': re.IGNORECASE,
}


@builder('^/(?P<pattern>.*)/(?P<flags>[i]*)$')
def build_filter_regex(pattern = '', flags = ''):
	"""
	Filter using a sed-style regex string.

	The supported flags are:
		i: ignore case

	/foo/ matches foo and foobar, but not FOO or bar
	/foo$/ matches foo but not foobar
	/foo/i matches FOO

	Examples
	--------
	>>> build_filter_regex('/foo/')('foo')
	True
	>>> build_filter_regex('/foo/i')('FOOBAR')
	True
	>>> build_filter_regex('/foo$/')('foobar')
	False
	"""
	flags = [REGEX_FLAGS[f] for f in flags]
	flags = reduce(lambda x, y: x | y, flags, 0)
	pattern = re.compile(pattern, flags)
	return lambda v: bool(pattern.search(safe_str(v)))


@builder('^(?P<script>.*[$].*)$')
def build_filter_eval(script = 'False'):
	"""
	Filter using a python statement, where the symbol $ stands in for the value being matched.

	$ > 5 to match anything larger than 5 (such as 6 or 100)
	'foo' in $ to match anything containing the string foo (such as 'foo', 'foobar' or ['foo', 'bar'])

	Examples
	--------
	>>> build_filter_eval('$ > 5')(10)
	True
	>>> build_filter_eval('$ > 5')(1)
	False
	"""
	return eval(f'lambda __var__: bool({script.replace("$", " __var__ ")})')


@builder('^(?P<search>.*)$')
def build_filter_contains(search = ''):
	"""
	Filter that will match values that contain the query, ignoring case.

	foo matches FOOBAR
	foo does not match bar

	Examples
	--------
	>>> build_filter_contains('foo')('FOOBAR')
	True
	>>> build_filter_contains('foo')('barfoo')
	True
	>>> build_filter_contains('foo')('bar')
	False
	"""
	search = search.lower()
	return lambda v: search in safe_str(v).lower()


BUILD_FILTER_METHODS = [
	build_filter_regex,
	build_filter_eval,
	build_filter_contains,
]


def build_filter_fuzzy(query):
	"""
	Build a fuzzy filter based on the input query.

	Attempts to build a filter using build_filter_regex, build_filter_numeric and build_filter_contains. The first one to
	succeed in building a filter will be used.

	Examples
	--------
	>>> build_filter_fuzzy('/^[a-z][0-9]$/')('a1')
	True
	>>> build_filter_fuzzy('$ > 5')(10)
	True
	>>> build_filter_fuzzy('f')('foo')
	True
	"""
	for build_filter_method in BUILD_FILTER_METHODS:
		filter_method = build_filter_method(query)
		if filter_method:
			return filter_method
	raise ValueError('Cannot create a filter from the provided query')

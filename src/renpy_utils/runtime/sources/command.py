# -*- coding: future_fstrings -*-

""" Extra commands available in the RenPy console. """

import argparse
import functools
import itertools
import shlex
import traceback

from renpy_utils.runtime.sources.filter import BUILD_FILTER_METHODS
from renpy_utils.runtime.sources.filter import build_filter_fuzzy
from renpy_utils.runtime.sources.scan import scan
from renpy_utils.util import format_docstring
from renpy_utils.util import get_sections
from renpy_utils.util import safe_str
from renpy_utils.util import truncate


# Will be set to the console in init()
_console = None


# Keep track of the commands that should be registered on the console
console_commands = {}


def command(usage, catch_exceptions = True):
	"""
	Add a command to the RenPy console.

	Extended version of the built-in @console decorator that bases the command on the usage string and shows stacktraces.
	"""
	def decorator(func):
		# Wrap to have better error handling
		@functools.wraps(func)
		def wrapper(l):
			try:
				return func(l)
			except Exception:
				if wrapper.catch_exceptions:
					return traceback.format_exc()
				else:
					raise

		# Get the command
		command = ''.join(itertools.takewhile(lambda c: c not in ': ', usage))

		# Setup the wrapper func
		wrapper.help = usage
		wrapper.inner = func
		wrapper.catch_exceptions = catch_exceptions

		# Register the command
		console_commands[command] = wrapper

		return wrapper
	return decorator


def get_docs(func):
	"""
	Get the docs of a function, for use in the RenPy console.

	If there is a section named 'Usage' in the docstring, it will be used.
	Otherwise, the main section of the docstring will be used.
	If there is no docstring at all, the summary from the usage will be used.
	"""
	if func.__doc__:
		sections = get_sections(func.__doc__)
		if 'Usage' in sections:
			return sections['Usage'].strip()
		else:
			return sections[''].strip()
	elif hasattr(func, 'help'):
		_, summary = func.help.split(':', 1)
		return summary.strip().capitalize()
	else:
		return 'Unknown'


# Create an extended version of the help command
@command('help [<command>]: this help, or help for a specific command')
def command_help(l):
	"""
	Display the available commands and their usage.

	When the name of a command is given, will display detailed usage information for this command, if available.
	"""
	# Check if a command was passes as an argument. If not, fallback to the default help command
	command = l.rest()
	if not command:
		return _console.help(l)

	# Get the command
	if command not in _console.config.console_commands:
		return f'Unknown command {command}'
	func = _console.config.console_commands[command]

	# Get usage and docs
	usage, _ = func.help.split(':', 1)
	docs = get_docs(func)

	return f'Usage: {usage.strip()}\n\n{docs}'


parser = argparse.ArgumentParser(
	prog = 'scan',
	description = 'inspect variables by name or value',
)
parser.add_argument(
	'query',
	nargs = argparse.REMAINDER,
	help = 'the query to base the filter on',
)
parser.add_argument(
	'-f',
	'--filter-on',
	action = 'store',
	choices = ['key', 'value', 'both'],
	default = 'both',
	help = 'the part of the variable that should be filtered on. default is both key and value',
)
parser.add_argument(
	'-t',
	'--target',
	action = 'store',
	default = None,
	help = 'the variable to scan in. default is the store',
)
parser.add_argument(
	'-p',
	'--page',
	action = 'store',
	default = 1,
	type = int,
	help = 'the page to show, if there are a lot of results',
)
parser.add_argument(
	'-r',
	'--recurse',
	action = 'store',
	default = 0,
	type = int,
	help = 'the amount of levels of nested objects to search through. will slow the search down considerably',
)


MAX_SCAN_LINE_LENGTH = 90
MAX_SCAN_LINES = 25


# The scan command
@command(f'{parser.format_usage().replace("usage: ", "").strip()}: {parser.description}')
@format_docstring(page_size = MAX_SCAN_LINES, docs = '\n\n'.join([get_docs(func) for func in BUILD_FILTER_METHODS]))
def command_scan(l):
	"""
	Scan the store using a filter based on the query passed to this command, and show the first {page_size} results.

	The following filter methods will be attempted, in order. By default, both the keys and the values will be matched
	against.

	{docs}
	"""
	# Parse the arguments
	input = l.rest()
	args = parser.parse_args(shlex.split(input))

	# Convert the query to a filter
	query = ' '.join(args.query)
	filter = build_filter_fuzzy(query)
	def safe_filter(v):
		""" Like filter, but converts TypeErrors to non-matches. """
		try:
			return filter(v)
		except TypeError:
			return False

	# Build the filter that will be used on scan
	scan_filter_methods = []
	if args.filter_on in ['key', 'both']:
		scan_filter_methods.append(lambda k, v: safe_filter(k))
	if args.filter_on in ['value', 'both']:
		scan_filter_methods.append(lambda k, v: safe_filter(v))
	def scan_filter(k, v):
		""" Apply safe_filter to the key/value/both, depending on the filter-at arg. """
		for scan_filter_method in scan_filter_methods:
			if scan_filter_method(k, v):
				return True
		return False

	# Extra filters that are not dependent on user input
	def crud_filter(k, v):
		""" Filter out internal stuff that usually just clutters up the results. """
		if safe_str(k).startswith('__'):
			return False
		return scan_filter(k, v)

	# Get the chosen variable
	target = _console.store
	if args.target:
		target = eval(args.target, vars(target))

	# Scan the target variable
	data = target
	data = scan(crud_filter, data, args.recurse)

	# Conver the result to output lines
	lines = sorted(data, key = lambda m: m.path)
	lines = [f'{m.path}: {m.value!r}' for m in lines]
	lines = [truncate(line, MAX_SCAN_LINE_LENGTH, '...') for line in lines]

	# Limit the amount of output lines
	text_more = f'and {len(lines) - MAX_SCAN_LINES + 1} more'
	lines = lines[(MAX_SCAN_LINES - 1) * (args.page - 1):]
	lines = truncate(lines, MAX_SCAN_LINES, [text_more])

	return '\n'.join(lines)


# A forcequit command
@command('forcequit: immediately close out of the game, without any prompts', catch_exceptions = False)
def command_forcequit(l):
	""" Exit the game, bypassing the normal "are you sure" prompt. """
	_console.renpy.quit()


# A restart command
@command('restart: immediately restart the game, without any prompts', catch_exceptions = False)
def command_restart(l):
	""" Exit and start a new instance of the game, bypassing the normal "are you sure" prompt. """
	_console.renpy.quit(relaunch = True)


def init(console):  # pragma: no cover
	""" Inject this module into a console. """
	global _console
	_console = console
	_console.config.console_commands.update(console_commands)

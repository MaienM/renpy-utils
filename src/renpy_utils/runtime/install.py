# -*- coding: future_fstrings -*-

""" The install action of the runtime components. """

import argparse
import hashlib
import io
import os
import os.path
import pathlib

from renpy_utils.util import Bunch
from renpy_utils.runtime.build import main as build_main


RENPY_GAME_DIR = 'game'


def readable_dir(path):
	""" Validate that a path points to a readable directory. """
	if not os.path.isdir(path):
		raise argparse.ArgumentTypeError(f"path does not point to a directory: '{path}'")
	elif not os.access(path, os.R_OK):
		raise argparse.ArgumentTypeError(f"path not readable: '{path}'")
	return path


def setup_parser(parser):
	""" Configure the parser for this module. """
	parser.add_argument(
		'-d',
		'--max-depth',
		help = (
			'The max amount of levels of directories to descend trying to find game folders to work on. '
			'1 means path/*/game, 2 also includes path/*/*/game, etc. '
			'Default 1.'
		),
		type = int,
		default = 1,
	)
	parser.add_argument(
		'-p',
		'--print',
		help = 'Print locations that were affected.',
		action = 'store_true',
		dest = 'print_paths',
	)
	parser.add_argument(
		'--dry-run',
		help = "Don't actually do anything, just print what would happen. Implies --print.",
		action = 'store_true',
	)
	parser.add_argument(
		'--force-write',
		help = "Don't check hashes of existing files, always assume the files need updating.",
		action = 'store_true',
	)
	parser.add_argument(
		'paths',
		help = "The path(s) to look in for RenPy games.",
		type = readable_dir,
		metavar = 'path',
		nargs = '+',
	)


def hash(data):
	""" Get a hash of the given data. """
	return hashlib.sha256(data.encode('ascii')).hexdigest()


def main(args):
	"""
	Install the runtime utilities into one or more instances.

	This creates a file using the build action, and places it into the game directory of any RenPy games it can find.
	"""
	if args.dry_run:
		args.print_paths = True

	# Find folders to install to
	game_dirs = []
	for path in args.paths:
		for dir, dirnames, filenames in os.walk(path):
			dir_path = pathlib.PurePath(dir).relative_to(path)
			depth = len(dir_path.parts)
			if RENPY_GAME_DIR in dirnames:
				game_dirs.append(os.path.realpath(os.path.join(dir, RENPY_GAME_DIR)))
				del dirnames[:]  # found a game dir, no need to recurse further
			elif depth >= args.max_depth:
				del dirnames[:]  # recursing further would violate the max-depth argument

	# Build the runtime file in memory
	build_script = ''
	if not args.dry_run:
		with io.StringIO() as f:
			build_main(Bunch(out = f))
			build_script = f.getvalue()
	build_script_hash = hash(build_script)

	# Write to game directories to install
	for game_dir in game_dirs:
		path = os.path.join(game_dir, 'renpy-utils.rpy')

		# Compare hashes to prevent needless re-writing of the same file
		reason = 'unknown'
		if args.force_write:
			reason = 'force'
		else:
			try:
				with open(path, 'r') as f:
					file_hash = hash(f.read())
					if file_hash == build_script_hash:
						continue
					reason = 'hash mismatch'
			except FileNotFoundError:
				reason = 'new'

		if args.print_paths:
			print(f'Writing to {path} ({reason})')
		if not args.dry_run:
			with open(path, 'w', encoding = 'UTF-8') as f:
				f.write(build_script)

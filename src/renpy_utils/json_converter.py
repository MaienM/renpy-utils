# -*- coding: future_fstrings -*-

"""
Utility methods to convert data to be suitable for JSON serialization.

The term JSON-safe is used throughout this module. What this means is comprised only of values that the built-in json
module can handle.
"""

from collections import OrderedDict


class ConversionFailure(object):
	"""
	Describes a failure in converting the data.

	Examples
	--------
	>>> failure = ConversionFailure(kind = 'list item', context = 'foo[2]', value = range(1, 10))
	>>> failure
	ConversionFailure<Could not convert list item of type <class 'range'> at foo[2] (range(1, 10))>
	>>> str(failure)
	"Could not convert list item of type <class 'range'> at foo[2] (range(1, 10))"
	"""

	def __init__(self, kind = 'value', context = '?', value = None):
		"""
		Create a new instance.

		Parameters
		----------
		kind : str
			A description of what kind of "thing" the value is. Eg value, list item, dict key.
		context : str
			A description of the context of the value (the rough path it takes to get to the value).
		value
			The value that failed to convert.
		"""
		self.kind = kind
		self.context = context
		self.value = value

	def __str__(self):
		""" Get a human-readable string representation of the failure. """
		return f'Could not convert {self.kind} of type {type(self.value)} at {self.context} ({self.value})'

	def __repr__(self):
		""" Get a human-readable string representation of the failure + the type. """
		return f'ConversionFailure<{self}>'


class Converter(object):
	"""
	Class that attempts to convert an object into a JSON-safe form.

	When it encounters keys and/or values that cannot be converted, it will log these and then continue, attempting to
	convert as much as possible.
	"""

	DEFAULT_TYPE_MAPPINGS = OrderedDict()
	DEFAULT_FALLBACKS = []

	def __init__(self, type_mappings = {}, fallbacks = []):
		"""
		Create a new instance of the JSON cleaner.

		Parameters
		----------
		type_mappings : dict
			A mapping of extra types that should be supported.

			The key is the type to support (subtypes will be supported as well), and the value is a function with the
			footprint documented on convert_none.

			The first type that matches will be used, so for multiple order might be important depending on the class
			structure. If this is the case, make sure to use an OrderedDict for compatibility with older python
			versions.
		fallbacks : list
			A list of functions that will attempt to convert any values that have not been handled by the type mappings.

			This could do things like use duck typing to convert things that cannot be detected by isinstance. These
			functions should have the same footprint as convert_none.

			If a fallback method fails (by raising an error), the next one will be tried. As soon as one succeeds this
			process will stop, and further fallback methods will not be used.
		"""
		# Build the type mappings, letting the user provided mappings go first, and not overriding these with our own
		self.type_mappings = OrderedDict()
		self.type_mappings.update(type_mappings)
		for k, v in Converter.DEFAULT_TYPE_MAPPINGS.items():
			self.type_mappings.setdefault(k, v)

		# Build the list of fallbacks, again letting the user provided ones go first
		self.fallbacks = fallbacks + Converter.DEFAULT_FALLBACKS

		# A list of ConversionFailure instances describing parts that could not be converted
		self.failures = []

	def convert(self, value, context = ''):
		"""
		Recursively convert a value into a JSON-safe form.

		Parameters
		----------
		value
			The value to convert into JSON-safe form.
		context : str
			A representation of the path of the current value. This can be used to provide the failure log with context.

		Returns
		-------
			The JSON-safe form of the value. Can be an incomplete representation of the input if parts failed to convert.
		"""
		for type_, converter in self.type_mappings.items():
			if isinstance(value, type_):
				try:
					return converter(value, self.convert, context, self.failures.append)
				except TypeError:
					self.failures.append(ConversionFailure(
						kind = f'using typemapping<{type_.__name__},{converter.__name__}>',
						context = context,
						value = value,
					))
		for converter in self.fallbacks:
			try:
				return converter(value, self.convert, context, self.failures.append)
			except TypeError:
				self.failures.append(ConversionFailure(
					kind = f'using fallback<{converter.__name__}>',
					context = context,
					value = value,
				))
		raise TypeError(f'{type(value).__name__} is not a JSON-safe type, and cannot be converted to one')

	@staticmethod
	def convert_none(value, convert, context, report):  # pragma: no cover
		"""
		Serve as an example of the footprint (custom) convert functions should have.

		Parameters
		----------
		value
			The value to convert into JSON-safe form.
		convert : func
			A method with the same footprint as the convert method of this class.
		context : str
			A representation of the path of the current value. This should be used to provide the failure reports with
			context.
		report : func
			A function accepting a ConversionFailure instance that can be used to report a non-critical failure has
			occurred. Use context to provide information on where the error occurred.

		Returns
		-------
			The JSON-safe form of the value.

			This can be an incomplete representation of the input if parts failed to convert.

			This does _not_ mean the return value should already be serialized, it only means that it should exclusively be
			made up of types that the built-in json module can serialize.

		Raises
		------
		TypeError
			If no data was converted.
		"""
		raise NotImplementedError()

	@staticmethod
	def _convert_passthrough(value, convert, context, report):
		return value

	@staticmethod
	def _convert_bytes(bytes, convert, context, report):
		return bytes.decode('UTF-8')

	@staticmethod
	def _convert_iterable(iterable, convert, context, report):
		result = []
		for i, value in enumerate(iterable):
			_context = f'{context}[{i}]'
			try:
				value = convert(value, _context)
			except TypeError:
				report(ConversionFailure(kind = 'list value', context = _context, value = value))
				continue
			result.append(value)
		return result

	@staticmethod
	def _convert_dict(dict, convert, context, report):
		result = {}
		for key, value in dict.items():
			try:
				key = convert(key, context)
			except TypeError:
				report(ConversionFailure(kind = 'dict key', context = context, value = key))
				continue
			_context = f'{context}.{key!r}'
			try:
				value = convert(value, _context)
			except TypeError:
				report(ConversionFailure(kind = 'dict value', context = _context, value = value))
				continue
			result[key] = value
		return result

	@classmethod
	def _fallback_iterable(cls, iterable, *args):
		return (
			f'Iterable extracted from object of type {type(iterable).__name__}',
			cls._convert_iterable(iter(iterable), *args),
		)

	@classmethod
	def _fallback_dict(cls, dict, *args):
		return (
			f'Dict extracted from object of type {type(dict).__name__}',
			cls._convert_dict(vars(dict), *args),
		)


# Types for which there is built-in support
Converter.DEFAULT_TYPE_MAPPINGS[str] = Converter._convert_passthrough
Converter.DEFAULT_TYPE_MAPPINGS[int] = Converter._convert_passthrough
Converter.DEFAULT_TYPE_MAPPINGS[float] = Converter._convert_passthrough
Converter.DEFAULT_TYPE_MAPPINGS[bool] = Converter._convert_passthrough
Converter.DEFAULT_TYPE_MAPPINGS[type(None)] = Converter._convert_passthrough
# Types for which we have our own methods
Converter.DEFAULT_TYPE_MAPPINGS[bytes] = Converter._convert_bytes
Converter.DEFAULT_TYPE_MAPPINGS[list] = Converter._convert_iterable
Converter.DEFAULT_TYPE_MAPPINGS[tuple] = Converter._convert_iterable
Converter.DEFAULT_TYPE_MAPPINGS[set] = Converter._convert_iterable
Converter.DEFAULT_TYPE_MAPPINGS[dict] = Converter._convert_dict
# Fallbacks for unknown types
Converter.DEFAULT_FALLBACKS += [
	Converter._fallback_iterable,
	Converter._fallback_dict,
]

#!/usr/bin/env python3
""" The entrypoint of the CLI for renpy-utils. """

from renpy_utils.parser import ModuleArgumentParser
import renpy_utils.main as main

if __name__ == '__main__':  # pragma: no cover
	parser = ModuleArgumentParser(main)
	parser.run()

"""
The main entrypoint of the application.

This imports the different modules, and provides a combined interface for them.
"""

import renpy_utils.runtime.main as runtime
import renpy_utils.saves.main as saves


SUBMODULES = {
	'runtime': runtime,
	'saves': saves,
}


HELP = "A suite of RenPy utilities."


def setup_parser(parser):
	""" Configure the parser for this module. """
	parser.add_subparsers(
		description = 'The module to do something with.',
		dest = 'module',
		metavar = 'module',
		modules = SUBMODULES,
	)

# -*- coding: future_fstrings -*-

""" Some utilities to make building and running our nested parsers easier. """

import argparse
import inspect

from renpy_utils.util import Bunch


class ModuleArgumentParser(argparse.ArgumentParser):
	"""
	A subclass of argparse.ArgumentParser that builds parsers from predefined methods in modules.

	For this to work, the modules that will be registered need to have a setup_parser function. This function takes a
	parser as argument, and sets it up by adding arguments to it.

	The module may also have a main function. If it does, this function will be called with the parsed args as argument.
	This function may be omitted for parsers that have a required subparser components, as it would serve no purpose in
	this scenario.

	Finally, the module may have a variable named HELP, which should be a string containing the description/help text for
	the module. If this is not present, the docstring of the main function will be used. If the module does not have a
	main function, the HELP variable is required.
	"""

	def __init__(self, module, *args, **kwargs):
		"""
		Create a new instance.

		Parameters
		----------
		module : module
			The module to create a parser for.
		"""
		if 'description' in kwargs:
			raise ValueError('description is not permitted, as it is provided by the setup_parser function')

		# Get module info
		self._module_info = self._preprocess_module(module)

		# Create the base parser
		super(ModuleArgumentParser, self).__init__(*args, description = self._module_info.description, **kwargs)

		# Setup
		self._module_info.setup_parser(self)
		self.set_defaults(main = self._module_info.main)

	def _default_main(self, args):
		raise NotImplementedError(f'The module {self._module_info.module.__name__} does not have a main method')

	def _preprocess_module(self, module):
		""" Pre-process a module, getting the important elements and validating it is usable for this class. """
		setup_parser = getattr(module, 'setup_parser', None)
		main = getattr(module, 'main', self._default_main)
		description = getattr(module, 'HELP', main.__doc__)
		description = description and inspect.cleandoc(description)

		if not setup_parser:
			raise ValueError('module must have a setup_parser function')
		if not description:
			raise ValueError('module must have either a main method with a docstring, or a variable named HELP.')

		return Bunch(module = module, setup_parser = setup_parser, description = description, main = main)

	def add_subparsers(self, *args, modules = None, required = False, **kwargs):
		"""
		Automatically builds subparsers from a dict of submodules.

		All extra arguments and keyword arguments will be passed to the original add_subparsers method.

		Parameters
		----------
		modules : dict
			The keys are strings representing the desired names of the parsers, and the values are the modules containing
			said parsers.
		required : bool
			Indicates whether the subparsers are a required parameter.

			If this is true, it means that the module defining this (non-sub)parser doesn't need a main function, as it
			will never be used.
		"""
		if not modules:
			raise ValueError('modules is required')

		subparsers = super(ModuleArgumentParser, self).add_subparsers(
			*args,
			parser_class = ModuleArgumentParser,
			**kwargs
		)
		subparsers.required = required
		for name, module in modules.items():
			module_info = self._preprocess_module(module)
			subparsers.add_parser(name, module = module, help = module_info.description)
		return subparsers

	def run(self, *args, **kwargs):
		""" Parse the arguments from the CLI, and then run the decided on main function. """
		parsed_args = self.parse_args(*args, **kwargs)
		return parsed_args.main(parsed_args)

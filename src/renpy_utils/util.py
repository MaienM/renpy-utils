# -*- coding: future_fstrings -*-

""" Miscelaneous utility methods. """

import inspect


class Bunch(object):
	"""
	A class that can be used to construct arbitrary objects.

	Examples
	--------
	>>> b = Bunch(foo=1, bar=True)
	>>> b.foo
	1
	>>> b.bar
	True
	>>> b.foobar
	Traceback (most recent call last):
		...
	AttributeError: 'Bunch' object has no attribute 'foobar'

	No new properties can be added after creation, but existing ones can be updated.

	>>> b.foo = 2
	>>> b.foo
	2
	>>> b.foobar = True
	Traceback (most recent call last):
		...
	AttributeError: 'Bunch' object has no attribute 'foobar'

	It is also possible to inherit the properties of another object.

	>>> c = Bunch(b, bar=False)
	>>> c.foo == b.foo
	True
	>>> c.bar == b.bar
	False
	>>> c.bar
	False
	"""

	def __init__(self, base = None, **kwargs):
		"""
		Create a new instance.

		All keyword arguments will be available as properties on the new instance.
		"""
		if base:
			self.__dict__.update(vars(base))
		self.__dict__.update(kwargs)

	def __setattr__(self, key, value):
		""" Make sure that no new properties are added. """
		if key not in self.__dict__:
			raise AttributeError(f"'{type(self).__name__}' object has no attribute '{key}'")
		super(Bunch, self).__setattr__(key, value)


def format_docstring(*args, **kwargs):
	"""
	Format the docstring of the decorated thing.

	This can be used to acomplish something similar to an f-string in older python versions.

	Parameters
	----------
	*args
		The arguments to pass to .format().
	**kwargs
		The keyword arguments to pass to .format().

	Returns
	-------
	func
		A decorator that mutates the docstring of the decorated object using .format with the given variables.

	Examples
	--------
	>>> @format_docstring(the_answer=42)
	... def example():
	...   "The answer is {the_answer}"
	>>> print(example.__doc__)
	The answer is 42
	"""
	def decorator(obj):
		# Old versions of RenPy (6.15) seem to strip docstrings, so do not treat the lack of one as an error.
		if obj.__doc__:
			obj.__doc__ = obj.__doc__.format(*args, **kwargs)
		return obj
	return decorator


def get_sections(docstring):  # noqa: D300
	"""
	Split the docstring into sections based on the section headers.

	Examples
	--------
	>>> def test():
	...   \"""
	...   Hello world.
	...
	...   Foo
	...   ---
	...   Foo stuff
	...   \"""
	>>> get_sections(test.__doc__)
	{'': 'Hello world.\\n', 'Foo': 'Foo stuff'}
	"""
	section_name = ''
	sections = { section_name: [] }
	lines = sections[section_name]
	for line in inspect.cleandoc(docstring).split('\n'):
		if len(lines) > 0 and line == ('-' * len(lines[-1])):
			# Complete header, so start a new section
			section_name = lines.pop()
			lines = sections[section_name] = []
		else:
			lines.append(line)
	for section_name, lines in sections.items():
		sections[section_name] = '\n'.join(lines)
	return sections


def truncate(iterable, max_length, replace_last = []):
	"""
	Truncate an iterable if it is too long.

	If the length of the iterable exceeds the max_length, it will be truncatated to this length, and the last element(s)
	will be replaced with replace_last.

	Examples
	--------
	>>> truncate('Lorem ipsum dolor sit amet', 100, '...')
	'Lorem ipsum dolor sit amet'
	>>> truncate('Lorem ipsum dolor sit amet', 25, '...')
	'Lorem ipsum dolor sit ...'
	>>> truncate(list(range(1, 100)), 5)
	[1, 2, 3, 4, 5]
	>>> truncate(list(range(1, 100)), 5, ['and more'])
	[1, 2, 3, 4, 'and more']
	"""
	if len(iterable) > max_length:
		return iterable[:max_length - len(replace_last)] + replace_last
	return iterable


def safe_str(obj):
	"""
	Get a string representation of an object.

	Unlike str/repr, this cannot throw an error; items for which no sensible string respresentation can be made will
	result in a fallback representation.
	"""
	methods = [str, repr, object.__repr__]
	for method in methods:
		try:
			return method(obj)
		except Exception:
			pass
	return 'Unable to convert to string'  # pragma: no cover


class Monkey(object):
	"""
	Temporarily overwrite a variable in a certain scope.

	Examples
	--------
	>>> class SpecialList(list):
	...   pass
	>>> l = SpecialList([1, 2, 3])
	>>> l.count(1)
	1
	>>> def rewritten(*args, **kwargs):
	...   return rewritten.original(*args, **kwargs) + 10
	>>> with Monkey() as m:
	...   rewritten.original = l.count
	...   m.setattr(l, 'count', rewritten)
	...   l.count(1)
	11
	>>> l.count(1)
	1
	"""

	VALUE_WAS_UNSET = object()

	def __init__(self):
		""" Create a new overwrite.  """
		self.attrs = []
		self.items = []

	def setattr(self, target, key, value):
		"""
		Set an attribute.

		Like setattr(target, key, value), but will be reversed automatically once this object is __exit__-ed.

		Parameters
		----------
		target
			The object containing the value to be overwritten.
		key : string
			The key of the value to be overwritten.
		value
			The value target.key should have within the scope of this overwrite.
		"""
		if hasattr(target, key):
			original_value = getattr(target, key)
		else:
			original_value = Monkey.VALUE_WAS_UNSET
		self.attrs.append((target, key, value, original_value))
		setattr(target, key, value)

	def setitem(self, target, key, value):
		"""
		Set an item.

		Like target[key] = value, but will be reversed automatically once this object is __exit__-ed.

		Parameters
		----------
		target
			The object containing the value to be overwritten.
		key : string
			The key of the value to be overwritten.
		value
			The value target.key should have within the scope of this overwrite.
		"""
		if key in target:
			original_value = target[key]
		else:
			original_value = Monkey.VALUE_WAS_UNSET
		self.items.append((target, key, value, original_value))
		target[key] = value

	def __enter__(self):
		""" Do nothing. """
		return self

	def __exit__(self, type, value, traceback):
		""" Undo the overwrites. """
		for (target, key, value, original_value) in self.attrs:
			if original_value == Monkey.VALUE_WAS_UNSET:
				delattr(target, key)
			else:
				setattr(target, key, original_value)
		for (target, key, value, original_value) in self.items:
			if original_value == Monkey.VALUE_WAS_UNSET:
				del target[key]
			else:
				target[key] = original_value

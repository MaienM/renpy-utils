from tests.fixtures.fixture_savefile import mock_save
from tests.fixtures.fixture_savefile import sample_save


collect_ignore = [
	'scripts/',
	'setup.py',
	'src/renpy_utils/runtime/sources/entrypoint.py',
	'src/renpy_utils/runtime/sources/ui.py',
	'src/renpy_utils/vendor/',
	'tests/integration/',
]


__all__ = (
	collect_ignore,
	mock_save,
	sample_save,
)
